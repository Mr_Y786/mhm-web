<?php

namespace app\http\middleware;


use think\facade\Cache;
use think\facade\Session;
use traits\controller\Jump;
use think\facade\Log;
use think\Request;

class AuthCheck
{
    use Jump;

    public function handle(Request $request, \Closure $next)
    {
        $action = strtolower($request->module()."/".$request->controller()."/".$request->action());
        //判断是否无需登录即可使用
        if(!in_array($action,config('auth.no_login_action'))){
            //根据session 判断是否已登录
            if(Session::has(SESSION_USER)){
                //判断是否多用户登陆
                if (Cache::get(CACHE_SESSION_ID.Session::get(SESSION_USER)[SESSION_USER_ID]) !== session_id()){
                    Session::clear();
                    return $this->error('当前帐号已在其他地方登陆！', 'index/login/index');
                }
                //进行权限判断
                if(!in_array($action,config('auth.no_auth_action'))){
                    //权限判断
                    $auth = new \Auth\Auth();
                    $uid = Session::get(SESSION_USER)[SESSION_USER_ID];
                    if (!$auth->check($action, $uid)) {// 第一个参数是规则名称,第二个参数是用户UID
                        Log::info(Session::get(SESSION_USER)[SESSION_USER_ID] . ' 访问 ' . $action . ' 权限验证失败！');
                        return $this->error('无访问权限！', 'index/login/index');
                    }
                }

//                if ($action != 'index/user/updatepwd'){
//                    if (Session::get(SESSION_USER)[SESSION_USER_PWDDATE] >= config('days')){
//                        return $this->error('密码已过期，请修改密码！', 'index/user/updatepwd');
//                    }
//                }
                return $next($request);
            }else{

                return $this->error('请先登录！','login/index');
            }

        }
        return $next($request);

    }
}

