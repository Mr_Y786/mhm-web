<?php

namespace app\http\middleware;

use think\Controller;
use think\Db;
use think\facade\Cache;
use think\Request;

class SvrCheck extends Controller
{
    public function handle(Request $request, \Closure $next)
    {
        $action = strtolower($request->module() . "/" . $request->controller() . "/" . $request->action());
        //判断是否无需登录即可使用
        if (!in_array($action, config('auth.no_login_action'))) {
            //判断openid
            $token = $request->header('weappAuth');
            if (empty($token)) {
                return err(2, ERR_2_MSG);
            } else {
                $user = Cache::remember('user_' . $token, function () use ($token) {
                    $re = Db::name('staff')
                        ->where('openid', '=', $token)
                        ->where('status', '=', STAFF_STATUS_ENABLE)
                        ->find();
                    return $re;
                });

                if (empty($user)) {
                    return err(2, ERR_2_MSG);
                }

                $request->user = $user;
            }
        }

        //判断是否需要json格式校验
        if (!in_array($action, config('auth.no_json_format_validate'))) {
            $input = file_get_contents('php://input');

            $data = '';

            if (!empty($input)) {
                //转换为数组
                $data = json_decode($input, true);
                if (empty($data)) {
                    return err(1, ERR_1_MSG);
                }
            }
            $request->postData = $data;
        }

        return $next($request);

    }
}
