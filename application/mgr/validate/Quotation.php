<?php


namespace app\mgr\validate;


use think\Validate;

class Quotation extends Validate
{
    protected $rule = [
        'area|装修面积'=>'require|max:10|between:1,9999999999',
        'decorate_type|建筑类型'=>'require',
        'decorate_class|装修类型'=>'require',
        'tel|手机号'=>'require|max:20',
    ];
}