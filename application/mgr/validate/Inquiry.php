<?php


namespace app\mgr\validate;


use think\Validate;

class Inquiry extends Validate
{
    protected $rule = [
        'bedroom|房屋户型' => 'require',
        'living|房屋户型' => 'require',
        'toilet|房屋户型' => 'require',
        'area|房屋面积'=>'require|max:30',
        'name|您的称呼'=>'require|max:30',
        'tel|您的电话'=>'require|max:20',
    ];
}