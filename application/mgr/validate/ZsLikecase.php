<?php


namespace app\mgr\validate;


use think\Validate;

class ZsLikecase extends Validate
{
    protected $rule = [
        'name|姓名'=>'require|max:15',
        'tel|手机号'=>'require|max:15',
        'area|面积'=>'require|max:15',
    ];
}