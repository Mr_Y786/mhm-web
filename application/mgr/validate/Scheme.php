<?php


namespace app\mgr\validate;


use think\Validate;

class Scheme extends Validate
{
    protected $rule = [
        'name|您的称呼'=>'require|max:20',
        'tel|您的联系方式'=>'require|max:20|number',
        'areaname|小区/楼盘名称'=>'require|max:30',
    ];
}