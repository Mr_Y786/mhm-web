<?php


namespace app\mgr\controller;


use Cassandra\Exception\ProtocolException;
use think\Controller;
use think\Db;
use think\db\Where;
use think\facade\Log;
use think\Request;
use think\Validate;

class Index extends Controller
{
    //装饰首页
    public function index(Request $request)
    {
//        $t1 = microtime(true);
        //增加网站访问记录
        get_site_data(2);
//        $t2 = microtime(true);
//        Log::info("1耗时" . round($t2 - $t1) . "秒");
//        return 123;
        $decorate = get_name_dict('decorate');

        //首页banner
        $banner = Db::name('news')
            ->where("find_in_set(2,putin)")
            ->where('status', '=', 1)
            ->where('show_kj', '=', 1)
            ->order('datetime', 'desc')
            ->field("banner as src,title as alt,id,type")
            ->limit(3)
            ->select();
        foreach ($banner as &$item) {
            $item['src'] = config('kj_url') . "/uploads/" . $item['src'];
        }
        $plan_count = Db::name('scheme')->where('status', 1)->where('plan', 1)->count();
//        $t3 = microtime(true);

//        Log::info("耗时" . round($t3 - $t2) . "秒");

        //首页新闻
        $where = new Where();
        $where['status'] = ['=', 1];
        $where['show_kj'] = ['=', 2];
        $where['recommended'] = ['=', 1];
        $where[] = ['exp', Db::raw('FIND_IN_SET(2,putin)')];
        $list = Db::name('news')
//            ->where("find_in_set(2,putin)")
            ->where($where)
            ->limit(0, 5)
            ->field('id,title,date,miaoshu,banner,alt')
            ->order('datetime', 'desc')
            ->select();
//        $t4 = microtime(true);

//        Log::info("耗时" . round($t4 - $t3) . "秒");
        foreach ($list as &$item) {
            $item['img'] = config('kj_url') . "/uploads/" . $item['banner'];
            $item['date'] = explode('-', $item['date']);
            $item['status'] = 1;
            unset($item['banner']);
        }
        if (count($list) !== 0) {
            $list[0]['status'] = 0;
        }
//        $t5 = microtime(true);
//        Log::info("耗时" . round($t5 - $t4) . "秒");
        //首页案例
        $case = Db::name('zs_case')
            ->where('status', 1)
            ->where('recommend', 1)
            ->order('datetime', 'desc')
            ->limit(0, 4)
            ->select();
        foreach ($case as &$item) {
            $item['img'] = config('kj_url') . "/uploads/" . $item['img'];
        }
//        $t6 = microtime(true);
//        Log::info("耗时" . round($t6 - $t5) . "秒");
        //首页视频
        $video = Db::name('video')
            ->where('status', 1)
            ->order('datetime', 'desc')
            ->limit(0, 3)
            ->select();
        foreach ($video as &$item) {
            $item['img'] = config('kj_url') . "/uploads/video/img/" . $item['img'];
            $item['src'] = config('kj_url') . "/uploads/video/" . $item['src'];
        }
        $t7 = microtime(true);
//        Log::info("耗时" . round($t7 - $t6) . "秒");
        $res = Db::name('zs_link')
            ->where('status', 1)
            ->select();
//        $t8 = microtime(true);
//        Log::info("耗时" . round($t8 - $t7) . "秒");
        $this->assign(
            [
                'banner' => $banner,
                'list' => $list,
                'case' => $case,
                'video' => $video,
                'decorate' => $decorate,
                'plan_count' => $plan_count,
                'res' => $res
            ]
        );

        return $this->fetch();
    }

    /**首页提交免费设计案例
     * @return false|string
     */
    public function plan()
    {
        $data = input('param.');
        $validate = new \app\mgr\validate\Scheme();
        if (!$validate->check($data)) {
            return $this->error($validate->getError());
        }
        $data['datetime'] = date('Y-m-d H:i:s');
        $data['date'] = date("Y-m-d");
        $res = Db::name('scheme')->strict(false)->insert($data);
        if ($res !== false) {
            return $this->success('提交成功！');
        } else {
            return $this->error('提交失败！');
        }
    }

    /**
     * 首页——我家装修要花多少钱？
     */
    public function inquiry()
    {
        $data = input('param.');
        $data['datetime'] = date("Y-m-d H:i:s");
        $data['date'] = date("Y-m-d");
        $Inquiry = new \app\mgr\validate\Inquiry;
        if (!$Inquiry->check($data)) {
            return $this->error($Inquiry->getError());
        }
        $res = Db::name('inquiry')
            ->strict(false)
            ->insert($data);
        if ($res !== false) {
            return $this->success("提交成功！");
        } else {
            return $this->error("提交失败！");
        }
    }

    /**
     * 在线预约
     */
    public function make()
    {
        $data = input('param.');
        $Validate = new Validate();
        $Validate->rule(
            [
                'name|姓名' => 'require|max:15',
                'tel|电话' => 'require|max:20',
            ]
        );
        if (!$Validate->check($data)) {
            return $this->error($Validate->getError());
        }
        $data['datetime'] = date("Y-m-d :H:i:s");
        $res = Db::name('zs_make')->strict(false)->insert($data);
        if ($res !== false) {
            return $this->success('提交成功！');
        } else {
            return $this->error('提交失败！');
        }
    }

    /**关于我们
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function about()
    {
        $res = Db::name('zs_site')
            ->where('id', 1)
            ->find();
        $this->assign(
            [
                'res' => $res
            ]
        );
        return $this->fetch();
    }

    /*精彩案例页
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function cases()
    {
        //获取页面查询数组
        $list = get_all_dict();
        $decorate_style[] = ['cate' => 'decorate_style', 'id' => '0', 'name' => '不限'];
        $building_type[] = ['cate' => 'building_type', 'id' => '0', 'name' => '不限'];
        $building_area[] = ['cate' => 'building_area', 'id' => '0', 'name' => '不限'];
        $price[] = ['cate' => 'price', 'id' => '0', 'name' => '不限'];

        foreach ($list as $item) {
            if ($item['cate'] == 'decorate_style') {
                array_push($decorate_style, $item);
            }
            if ($item['cate'] == 'building_type') {
                array_push($building_type, $item);
            }
            if ($item['cate'] == 'building_area') {
                if ($item['id'] == 1) {
                    $item['name'] = "<{$item['name']}m²";
                } else {
                    if ($item['id'] == 6) {
                        $item['name'] = ">{$item['name']}m²";
                    } else {
                        $array = explode(',', $item['name']);
                        $item['name'] = $array[0] . "m²" . "~" . $array[1] . "m²";
                    }
                }
                array_push($building_area, $item);
            }
            if ($item['cate'] == 'price') {
                if ($item['id'] == 1) {
                    $item['name'] = "<{$item['name']}万元";
                } else {
                    if ($item['id'] == 6) {
                        $item['name'] = ">{$item['name']}万元";
                    } else {
                        $array = explode(',', $item['name']);
                        $item['name'] = $array[0] . "万元" . "~" . $array[1] . "万元";
                    }
                }
                array_push($price, $item);
            }
        }

        //构造查询条件
        $where = new Where();
        //建筑类型
        $building_type_id = input('param.building_type', '0');
        if ($building_type_id !== '0') {
            $where['building_type_id'] = ['=', $building_type_id];
        }
        //装修风格
        $decorate_style_id = input('param.decorate_style', '0');
        if ($decorate_style_id !== '0') {
            $where['decorate_style_id'] = ['=', $decorate_style_id];
        }
        //建筑面积
        $building_area_id = input('param.building_area', '0');
        if ($building_area_id !== '0') {
            $areastr = get_id_name_dict('building_area', $building_area_id);
            if ($building_area_id == '1') {
                $where['building_area_id'] = ['<', $areastr];
            } else {
                if ($building_area_id == '6') {
                    $where['building_area_id'] = ['>', $areastr];
                } else {
                    $where['building_area_id'] = ['between', $areastr];
                }
            }
        }
        //价格区间
        $price_id = input('param.price', '0');
        if ($price_id !== '0') {
            $pricestr = get_id_name_dict('price', $price_id);
            if ($price_id == '1') {
                $where['price_id'] = ['<', $pricestr];
            } else {
                if ($price_id == '6') {
                    $where['price_id'] = ['>', $pricestr];
                } else {
                    $where['price_id'] = ['between', $pricestr];
                }
            }
        }
        //排序
        $orders = input('param.order', '1');
        if ($orders == '1') {
            $order = ['datetime' => 'desc'];
        } else {
            if ($orders == '2') {
                $order = ['browse' => 'desc', 'datetime' => 'desc'];
            } else {
                if ($orders == '3') {
                    $order = ['browse' => 'desc'];
                }
            }
        }

        //查询
        $value = input('param.value', '');
        if (!empty($value)) {
            $where['title'] = ['like', "%$value%"];
        }

        $where['status'] = ['=', 1];
        $list = Db::name('zs_case')
            ->where($where)
            ->order($order)
            ->paginate(9, false, ['query' => input('param.')])
            ->each(
                function ($item, $key) {
                    $item['img'] = config('kj_url') . "/uploads/" . $item['img'];
                    $item['building_type_id'] = get_id_name_dict('building_type', $item['building_type_id']);
                    $item['decorate_style_id'] = get_id_name_dict('decorate_style', $item['decorate_style_id']);
                    return $item;
                }
            );


        $this->assign(
            [
                'list' => $list,
                'decorate_style' => $decorate_style,
                'building_type' => $building_type,
                'building_area' => $building_area,
                'price' => $price,
                'decorate_style_id' => $decorate_style_id,
                'building_type_id' => $building_type_id,
                'building_area_id' => $building_area_id,
                'price_id' => $price_id,
                'order' => $orders
            ]
        );
        return $this->fetch();
    }

    /**案例详情
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function cases_page()
    {
        $id = input('param.id');
        $res = Db::name('zs_case')
            ->where('id', $id)
            ->find();
        $res['img'] = config('kj_url') . "/uploads/" . $res['img'];
        $res['building_type_id'] = get_id_name_dict('building_type', $res['building_type_id']);
        $res['decorate_style_id'] = get_id_name_dict('decorate_style', $res['decorate_style_id']);

//        $bid = $res['decorate_style_id'];

        //案例推荐
        $case = Db::name('zs_case')
            ->where('status', 1)
            ->where('recommend', 1)
            ->order('datetime', 'desc')
            ->limit(0, 5)
            ->select();
        foreach ($case as &$item) {
            $item['img'] = config('kj_url') . "/uploads/" . $item['img'];
            $item['building_type_id'] = get_id_name_dict('building_type', $item['building_type_id']);
            $item['decorate_style_id'] = get_id_name_dict('decorate_style', $item['decorate_style_id']);
        }

        //猜你喜欢—按阅读量排序
        $list = Db::name('zs_case')
            ->where('status', 1)
            ->where('id', '<>', $id)
            ->order(['browse' => 'desc', 'datetime' => 'desc'])
            ->limit(0, 5)
            ->select();

        foreach ($list as &$item) {
            $item['img'] = "http://maohemao.com/uploads/" . $item['img'];
            $item['building_type_id'] = get_id_name_dict('building_type', $item['building_type_id']);
            $item['decorate_style_id'] = get_id_name_dict('decorate_style', $item['decorate_style_id']);
        }
        //按风格提交人数
        $count = Db::name('zs_likecase')
            ->where('status', 1)
            ->count();

        $this->assign(
            [
                'res' => $res,
                'case' => $case,
                'list' => $list,
                'count' => $count
            ]
        );
        return $this->fetch();
    }

    public function likecases()
    {
        $data = input('param.');
        $validate = new \app\mgr\validate\ZsLikecase();
        if (!$validate->check($data)) {
            return $this->error($validate->getError());
        }
        $data['datetime'] = date("Y-m-d H:i:s");
        $res = Db::name('zs_likecase')->strict(false)->insert($data);
        if ($res !== false) {
            return $this->success('提交成功！');
        } else {
            return $this->error('提交失败！');
        }
    }

    /**资讯动态
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function news()
    {
        $where = new Where();
        $where['status'] = ['=', 1];
//        $where['show_kj'] = ['=',2];
        $where[] = ['exp', Db::raw('FIND_IN_SET(2,putin)')];

        $typeid = input('param.typeid', 4);
        if ($typeid != 4) {
            $where['type'] = ['=', $typeid];
        }

        //查询
        $value = input('param.value', '');
        if (!empty($value)) {
            $where['title'] = ['like', "%$value%"];
        }

        $list = Db::name('news')
            ->where($where)
            ->field('id,title,date,miaoshu,banner,alt,grab')
            ->order('datetime', 'desc')
            ->paginate(6, false, ['query' => input('param.')])
            ->each(function ($item, $key) {
                    if ($item['grab'] == 2) {
                        $item['img'] = config('kj_url') . "/uploads/" . $item['banner'];
                    } else {
                        $item['img'] = $item['banner'];
                    }
                    unset($item['banner']);
                    return $item;
                }
            );

        //行业资讯推荐
        $wheres = new Where();
        $wheres['status'] = ['=', 1];
//        $wheres['show_kj'] = ['=',2];
        $wheres['type'] = ['=', 1];
        $wheres[] = ['exp', Db::raw('FIND_IN_SET(2,putin)')];
        $res = Db::name('news')
            ->where($wheres)
            ->field('id,title,miaoshu,banner,alt,type,grab')
            ->limit(0, 4)
            ->order('datetime', 'desc')
            ->select();
        foreach ($res as &$item) {
            if ($item['grab'] == 2) {
                $item['img'] = config('kj_url') . "/uploads/" . $item['banner'];
            } else {
                $item['img'] = $item['banner'];
            }
            unset($item['banner']);
        }

        //装修指南推荐
        $whereas = new Where();
        $whereas['status'] = ['=', 1];
//        $whereas['show_kj'] = ['=',2];
        $whereas['type'] = ['=', 3];
        $whereas[] = ['exp', Db::raw('FIND_IN_SET(2,putin)')];

        $resl = Db::name('news')
            ->where($whereas)
            ->field('id,title,miaoshu,banner,alt,type,grab')
            ->limit(0, 4)
            ->order('datetime', 'desc')
            ->select();
        foreach ($resl as &$item) {
            if ($item['grab'] == 2) {
                $item['img'] = config('kj_url') . "/uploads/" . $item['banner'];
            } else {
                $item['img'] = $item['banner'];
            }
            unset($item['banner']);
        }


        $type = get_name_dict('new_type');
        array_unshift($type, ['id' => '4', 'name' => '全部动态']);

        $this->assign(
            [
                'list' => $list,
                'res' => $res,
                'resl' => $resl,
                'type' => $type,
                'typeid' => $typeid
            ]
        );
        return $this->fetch();
    }

    public function news_page()
    {
        $id = input('param.id');
        $typeid = input('param.typeid', '4');

        $res = Db::name('news')->where('id', $id)->find();

        $where = new Where();
        $where['status'] = ['=', 1];
        $where['datetime'] = ['>', $res['datetime']];
        $where[] = ['exp', Db::raw('FIND_IN_SET(2,putin)')];
        if ($typeid != '4') {
            $where['type'] = ['=', $typeid];
        }

        //上一篇
        $up = Db::name('news')
            ->where($where)
            ->order('datetime', 'asc')
            ->limit(0, 1)
            ->find();

        $wher = new Where();
        $wher['status'] = ['=', 1];
        $wher['datetime'] = ['<', $res['datetime']];
        $wher[] = ['exp', Db::raw('FIND_IN_SET(2,putin)')];
        if ($typeid != '4') {
            $wher['type'] = ['=', $typeid];
        }
        //下一篇
        $down = Db::name('news')
            ->where($wher)
            ->order('datetime', 'desc')
            ->limit(0, 1)
            ->find();

        //行业资讯推荐
        $wheres = new Where();
        $wheres['status'] = ['=', 1];
        $wheres['type'] = ['=', 1];
        $wheres['id'] = ['<>', $id];
        $wheres[] = ['exp', Db::raw('FIND_IN_SET(2,putin)')];
        $hangye = Db::name('news')
            ->where($wheres)
            ->field('id,title,miaoshu,banner,alt,type,grab')
            ->limit(0, 4)
            ->order('datetime', 'desc')
            ->select();
        foreach ($hangye as &$item) {
            if ($item['grab'] == 2) {
                $item['img'] = config('kj_url') . "/uploads/" . $item['banner'];
            } else {
                $item['img'] = $item['banner'];
            }
            unset($item['banner']);
        }

        //装修指南推荐
        $whereas = new Where();
        $whereas['status'] = ['=', 1];
        $whereas['type'] = ['=', 3];
        $whereas['id'] = ['<>', $id];
        $whereas[] = ['exp', Db::raw('FIND_IN_SET(2,putin)')];

        $zhinan = Db::name('news')
            ->where($whereas)
            ->field('id,title,miaoshu,banner,alt,type')
            ->limit(0, 4)
            ->order('datetime', 'desc')
            ->select();
        foreach ($zhinan as &$item) {
            $item['img'] = config('kj_url') . "/uploads/" . $item['banner'];
            unset($item['banner']);
        }

        $this->assign(
            [
                'res' => $res,
                'hangye' => $hangye,
                'zhinan' => $zhinan,
                'up' => $up,
                'down' => $down
            ]
        );
        return $this->fetch();
    }

    /**工程报价——获取建筑类型数组
     * @return array
     */
    public function aaa()
    {
        $arr = get_name_dict('building_type');
        $arr1 = array_filter(
            $arr,
            function ($item) {
                return $item['status'] == 1;
            }
        );
        $arr2 = array_values(
            array_filter(
                $arr,
                function ($item) {
                    return $item['status'] == 2;
                }
            )
        );
        return ['arr1' => $arr1, 'arr2' => $arr2];
    }

    /**工程报价
     * @param Request $request
     * @return array|mixed|void
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function offer(Request $request)
    {
        if ($request->isGet()) {
            $list = Db::name('quotation')->where('status', 1)->order('datetime', 'desc')->limit(0, 20)->select();
            foreach ($list as &$item) {
                $item['price'] = floatval($item['price']);
                $item['tel'] = substr_replace($item['tel'], '****', 3, 4);
            }
            $decorate_class = get_name_dict('decorate_class');
            $this->assign(
                [
                    'decorate_class' => $decorate_class,
                    'list' => $list
                ]
            );
            return $this->fetch();
        }

        if ($request->isPost()) {
            $data = input('param.');
            $validate = new \app\mgr\validate\Quotation();
            if (!$validate->check($data)) {
                return $this->error($validate->getError());
            }

            $buiding = Db::name('building_type')->where('id', $data['decorate_type'])->find();
            //建筑类型 ——decorate_type
            //装修类型 ——decorate_style
            //装修档次 ——decorate_class
            $price = $buiding['price'];
            $phase = $buiding['phase'] * $data['decorate_class'];
            $type = get_id_name_dict('building_type', $data['decorate_type']);

            if ($data['decorate_style'] == 1) {
                $style = '家装';
            } else {
                $style = '工装';
            }
            $count = ($phase + $price) * $data['area'];

            $data['datetime'] = date("Y-m-d H:i:s");
            $data['price'] = ($count) / 10000;

            $res = Db::name('quotation')->strict(false)->insert($data);
            if ($res !== false) {
                return [
                    'code' => 1,
                    'msg' => '提交成功',
                    'count' => $count,
                    'list' => [
                        'area' => $data['area'],
                        'type' => $type,
                        'style' => $style,
                        'class' => get_id_name_dict('decorate_class', $data['decorate_class'])
                    ]
                ];
            } else {
                return ['code' => 2, 'msg' => '提交失败', 'count' => ''];
            }
        }
    }

    /**装修视频
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function video()
    {
        //查询
        $value = input('param.value', '');
        if (!empty($value)) {
            $where['title'] = ['like', "%$value%"];
        }
        $where['status'] = ['=', 1];
        $list = Db::name('video')
            ->where($where)
            ->order('datetime', 'desc')
            ->field('title,src,img')
            ->paginate(4, false, ['query' => input('param.')])
            ->each(
                function ($item, $key) {
                    $item['src'] = config('kj_url') . "/uploads/video/" . $item['src'];
                    $item['img'] = config('kj_url') . "/uploads/video/img/" . $item['img'];
                    return $item;
                }
            );
        $this->assign(
            [
                'list' => $list
            ]
        );
        return $this->fetch();
    }
}