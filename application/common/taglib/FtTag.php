<?php
/**
 * Created by PhpStorm.
 * User: mhm
 * Date: 2018/9/1
 * Time: 17:16
 */

namespace app\common\taglib;


use think\template\TagLib;

class FtTag extends TagLib
{
    // 标签定义
    protected $tags = [
        // 标签定义： attr 属性列表 close 是否闭合（0 或者1 默认1） alias 标签别名 level 嵌套层次
        'areasel'     => ['attr' => 'name,selDefault','close'=>0],
        'php2'        => ['attr' => ''],
        'sel'       =>['attr'=>'name,src,selDefault','close'=>0],
    ];

    /**
     * areaSel
     * 格式：
     * {php}echo $name{/php}
     * @access public
     * @param  array $tag 标签属性
     * @param  string $content 标签内容
     * @return string
     */
    public function tagAreasel($tag, $content)
    {
        $name = $tag['name'];
        if(empty($name)){
            $name = 'filterArea';
        }

        $selDefault = $tag['selDefault'];
        if(empty($selDefault)){
            $selDefault = 0;
        }

        $list = getCacheArea('130100');
        //市局可以查看区县局数据，区县只可查看自己的。
        $userAreaID = session(SESSION_USER)[SESSION_DEPT_AREA_ID];
        if($userAreaID!='130100'){
            $list = array_values(array_filter($list,function($var) use ($userAreaID){
                return $var['id'] == $userAreaID;
            }));
        }else{
            $list = array_merge([['id'=>'0','name'=>'全部地区']],$list);
        }

        $parseStr = '<select class="form-control" name="'.$name.'">';
        foreach ($list as $item){
            $parseStr = $parseStr .'<option value="'.$item['id'].'" <?php if('.$item['id'].'=='.$selDefault.'){?>selected<?php }?>>'.$item['name'].'</option>';
        }
        $parseStr = $parseStr . '</select>';
        return $parseStr;
    }

    public function tagPhp2($tag, $content)
    {
        $parseStr = '<span style="font-weight: bold"> ' . $content . ' </span>';
        return $parseStr;
    }

    /** sel 通用下拉选择
     * @param $tag
     * @param $content
     */
    public function tagSel($tag,$content){
        $name = $tag['name'];
        if(empty($name)){
            $name = 'filterArea';
        }

        $selDefault = $tag['selDefault'];
        if(empty($selDefault)){
            $selDefault = 0;
        }

        $src = $this->autoBuildVar($tag['src']);

        $parseStr = '<?php ';
        $parseStr .= ' $__AJ_LIST__ = ' . $src . ';?>';
        $parseStr .= '<select class="form-control" name="'.$name.'">';
        $parseStr .= '<?php foreach($__AJ_LIST__ as $item) { ?> ';
        $parseStr .= ' <option <?php if($item[\'id\']=='.$selDefault.'){?>selected<?php }?> value="<?php echo $item[\'id\'] ?>"><?php echo $item[\'name\'] ?></option>';
        $parseStr .= ' <?php }?>';
        $parseStr .= '</select>';
        return $parseStr;
    }
}
