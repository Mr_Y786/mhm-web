<?php
/**
 * Created by PhpStorm.
 * User: mhm
 * Date: 2018/10/23
 * Time: 14:44
 */

namespace app\common\controller;


use think\Db;
use think\db\Where;
use think\facade\Cache;
use think\facade\Log;

class Notify
{
    const CACHE_NOTIFY_CNT = 'notify_cnt_';


    /**
     * 入库保存
     * 一般由队列调用，不直接被城程序调用
     */
    public static function save($uid,$title,$content,$url,$extra=null){
        \app\common\model\Notify::create(
            ['uid'=>$uid,'title'=>$title,'content'=>$content,
                'url'=>$url,'extra'=>$extra
            ]
        );
        Cache::rm(self::CACHE_NOTIFY_CNT.$uid);
    }

    public static function add($uid,$title,$content,$url,$extra=null){
        \think\Queue::push('SaveNotifyJob',['uid'=>$uid,'title'=>$title,'url'=>$url,'content'=>$content,'extra'=>$extra]);
    }



    /** 获取用户未读消息数
     * @param $uid
     * @param int $isEnt
     * @return mixed
     */
    public static function unreadCnt($uid=null){
        if(empty($uid)){
            $uid = get_user_info()[SESSION_USER_ID];
        }
        if(empty($uid)){
            return 0;
        }
        //cache key notify_cnt_uid
        return Cache::remember(self::CACHE_NOTIFY_CNT.$uid,function () use($uid){
            return Db::query("select count(*) cnt from mhm_notify where is_read=2 and uid=?",[$uid])[0]['cnt'];
        });
    }

    /** 读取全部消息
     * @param $uid
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public static function readAll($uid){
        $where = new Where;
        $where['uid']=$uid;
        $where['is_read']=2;
        Db::name('notify')->where($where)->update(['is_read'=>1,'read_time'=>date('Y-m-d H:i:s')]);
        //设置缓存
        Cache::rm(self::CACHE_NOTIFY_CNT.$uid);
    }


}
