<?php
/**
 * Created by PhpStorm.
 * User: mhm
 * Date: 2018/11/12
 * Time: 14:42
 */

namespace app\common\controller;


use think\Db;
use think\facade\Log;
use think\facade\Request;

class OpLog
{

    const CATE_COMMON=1;
    const CATE_LOGIN=2;
    const CATE_LOGOUT=3;

    //部门信息操作
    const CATE_DEPT_ADD=4;
    const CATE_DEPT_UPDATE=5;
    const CATE_DEPT_DEL=6;

    //用户信息操作
    const CATE_USER_ADD=7;
    const CATE_USER_UPDATE=8;
    const CATE_USER_DEL=9;

    //个人信息操作
    const CATE_USER_SELF_UPDATE=10; //修改个人信息
    const CATE_USER_SELF_PWD=11; //修改登录密码


    //栏目操作
    const CATE_CATE_ADD=12;
    const CATE_CATE_DEL=13;
    const CATE_CATE_UPDATE=14;


    //内容操作
    const CATE_CMS_ADD=15;
    const CATE_CMS_DEL=16;
    const CATE_CMS_UPDATE=17;
    const CATE_CMS_AUDIT=18;
    const CATE_CMS_UNAUDIT=19;
    const CATE_CMS_SHOW=20;
    const CATE_CMS_UNSHOW=21;


    //外链操作
    const CATE_OUTCATE_UPDATE=31;
    const CATE_WECHAT_UPDATE=32;

    //友情链接操作
    const CATE_LINK_ADD=41;
    const CATE_LINK_DEL=42;
    const CATE_LINK_UPDATE=43;


    const CATE_OTHER=99;



    /** 入库日志
     * @param $uid 用户id
     * @param $remark 操作内容
     * @param $url action
     * @param int $cate 操作类别
     * @param null $extra 扩展信息
     */
    public static function save($uid,$remark,$url,$opTime,$cate=self::CATE_OTHER,$extra=null){
        \app\common\model\OpLog::create(['uid'=>$uid,'remark'=>$remark,'url'=>$url,'cate'=>$cate,'extra'=>$extra,'op_time'=>$opTime]);
    }

    /** 日志入队列，逐条处理。
     * @param $uid
     * @param $remark
     * @param $url
     * @param int $cate
     * @param null $extra
     */
    public static function add($uid,$remark,$url,$cate=self::CATE_OTHER,$extra=null){
        $opTime = date('Y-m-d H:i:s');
        \think\Queue::push('SaveOpLogJob',['uid'=>$uid,'remark'=>$remark,'url'=>$url,'opTime'=>$opTime,'cate'=>$cate,'extra'=>$extra]);
    }

    public static function commonAdd($remark,$cate=self::CATE_COMMON,$extra=null){
        Log::info('commonAdd invoked!');
        $userInfo = get_user_info();
        if(!empty($userInfo)){
            Log::info('commonAdd add!');
            $uid = get_user_info()[SESSION_USER_ID];
            $url = Request::path();
            self::add($uid,$remark,$url,$cate,$extra);
        }

    }


}
