<?php

namespace app\common\validate;

use think\Validate;

class Login extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
	protected $rule = [
        'login_name|用户名' => 'require',
        'authKey|密码' => 'require',
//        'sms|短信验证码'=>'require',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [];

    protected $scene = [
        'sms'  =>  ['login_name','passwd'],
        'login'=>['login_name','authKey','captcha']
    ];
}
