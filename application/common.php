<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
//缓存常量
const CACHE_RULE_LIST = 'cache_rule';//全部权限节点
const CACHE_DICT_LIST = 'cache_dict';//字典表缓存
const CACHE_AREA_LIST = 'cache_area';//区域缓存
const CACHE_CFG='cache_cfg';//配置信息（外联网址，微信appid等。）



/**
 * 配置内容
 */
function get_all_cfg(){
    return \think\facade\Cache::remember(CACHE_CFG,function(){
        return \think\Db::query("select * from mhm_cfg");
    });
}

//获取某一类别的cfg信息
/**
 * @param int $cate
 * @return array|mixed
 */
function get_cfg_by_cate($cate=1){
    $listCfg = get_all_cfg();
    $list = array_filter($listCfg,function($item) use($cate){
        return $item['cate']==$cate;
    });
    return $list;
}

/** 根据key获取cfg配置信息
 * @param $name
 * @return 0|string
 */
function get_cfg_by_name($name){
    if(empty($name)){
        return '';
    }

    $listCfg = get_all_cfg();
    $list = array_values(array_filter($listCfg,function($item) use($name){
        return $item['key'] == $name;
    }));

    if(empty($list)){
        return '';
    }else{
        return $list[0];
    }
}

/**二维数组排序
 * @param $array 数组
 * @param $field 排序字段
 * @param string $sort 升序降序，
 * @return mixed
 */
function array_sequence($array, $field, $sort = 'SORT_ASC')
{
    $arrSort = array();
    foreach ($array as $uniqid => $row) {
        foreach ($row as $key => $value) {
            $arrSort[$key][$uniqid] = $value;
        }
    }
    array_multisort($arrSort[$field], constant($sort), $array);
    return $array;
}


/**根据父节点编号 构造菜单树
 * @param $pid
 * @return array|mixed
 */
function prepare_tree_menu($pid,$listRuleOri){
    $listRule = array_filter($listRuleOri,[ new \ftkj\TreeFilter($pid),'getChild']);
    if(count($listRule)>0){
        $listRule = array_sequence($listRule,'sort');
    }

    foreach ($listRule as &$item){
        $listChildRule = prepare_tree_menu($item['id'],$listRuleOri);
        if(count($listChildRule)>0){
            $item['children'] = $listChildRule;
        }
    }
    return $listRule;
}

/** 宽字符补齐
 * @param $str
 * @param $pad_len
 * @param string $pad_str
 * @param int $dir
 * @param null $encoding
 * @return string
 */
function mb_str_pad($str, $pad_len, $pad_str = ' ', $dir = STR_PAD_RIGHT, $encoding = NULL)
{
    $encoding = $encoding === NULL ? mb_internal_encoding() : $encoding;
    $padBefore = $dir === STR_PAD_BOTH || $dir === STR_PAD_LEFT;
    $padAfter = $dir === STR_PAD_BOTH || $dir === STR_PAD_RIGHT;
    $pad_len -= mb_strlen($str, $encoding);
    $targetLen = $padBefore && $padAfter ? $pad_len / 2 : $pad_len;
    $strToRepeatLen = mb_strlen($pad_str, $encoding);
    $repeatTimes = ceil($targetLen / $strToRepeatLen);
    $repeatedString = str_repeat($pad_str, max(0, $repeatTimes)); // safe if used with valid utf-8 strings
    $before = $padBefore ? mb_substr($repeatedString, 0, floor($targetLen), $encoding) : '';
    $after = $padAfter ? mb_substr($repeatedString, 0, ceil($targetLen), $encoding) : '';
    return $before . $str . $after;
}

/** 获取子节点。
 * @param $list
 * @param $pid
 * @return array
 */
function get_child($list, $pid){
    return array_filter($list,[ new \ftkj\TreeFilter($pid),'getChild']);
}

/** 获取字典表全部信息
 * @return mixed
 */
function get_all_dict(){
    return \think\facade\Cache::remember(CACHE_DICT_LIST,function(){
        return \think\Db::name('dict')->all();
    });
}
/**获取字典表某类下的信息
 * @param $cate
 * @return array
 */
function get_name_dict($cate){
    $list = get_all_dict();
    $cates = array_values(array_filter($list,function ($v) use ($cate){
        return ($v['cate'] == $cate);
    }));
    return $cates;
}

function get_id_name_dict($cate,$id){
    $list = get_all_dict();
    $cates = array_values(array_filter($list,function ($v) use ($cate,$id){
        return ($v['cate'] == $cate && $v['id'] == $id);
    }));
    \think\facade\Log::info("====================".json_encode($id));
    return $cates[0]['name'];
}

/**
 * 获取网站访问用户详细信息
 * IP地址、地区、时间、日期、来源网址（直接访问的没有此项）
 */
function get_site_data($type){
    $data['ip'] = $_SERVER['REMOTE_ADDR'];
    $data['datetime'] = date("Y-m-d H:i:s");
    $data['date'] = date("Y-m-d");
    $data['site_type'] = $type;
    if (!empty($_SERVER['HTTP_REFERER'])){
        preg_match("/\w+:\/\/(\w+.\w+.\w+)/",$_SERVER['HTTP_REFERER'],$host);
        $data['source'] = $host[0];
    }
//    $addr = get_area($data['ip']);
//    if ($addr['code'] == 0){
//        $data['country'] = $addr['data']['country'];
//        $data['region'] = $addr['data']['region'];
//        $data['city'] = $addr['data']['city'];
//        $data['county'] = $addr['data']['county'];
//    }

//     $sta = \think\Db::name('data')->where(['date'=>$data['date'],'ip'=>$data['ip']])->find();
//     if (empty($sta)){
//         \think\Db::name('data')->strict(false)->insert($data);
//     }
    \think\Queue::push('RecordJob',$data,'RecordJobs');
}
/**网页访问量调价到数据库——进程调用
 * @param $data
 * @throws \think\db\exception\DataNotFoundException
 * @throws \think\db\exception\ModelNotFoundException
 * @throws \think\exception\DbException
 */
function recordjob($data){
    $sta = \think\Db::name('data')->where(['date'=>$data['date'],'ip'=>$data['ip']])->where('site_type',$data['site_type'])->find();
    if (empty($sta)){
        \think\Db::name('data')->strict(false)->insert($data);
    }
}

/**根据IP获取地区
 * 淘宝接口
 * @param $ip
 * @return mixed
 */
function get_area($ip){
    $url = "http://ip.taobao.com/service/getIpInfo.php?ip={$ip}";
    $ret = https_request($url);
    $arr = json_decode($ret,true);
    return $arr;
}
/**根据IP获取地区名——POST请求函数
 * @param $url
 * @param null $data
 * @return bool|string
 */
function https_request($url,$data = null){
    $curl = curl_init();
    curl_setopt($curl,CURLOPT_URL,$url);
    curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,false);
    curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,false);
    if(!empty($data)){
        //如果有数据传入数据
        curl_setopt($curl,CURLOPT_POST,1);//CURLOPT_POST 模拟post请求
        curl_setopt($curl,CURLOPT_POSTFIELDS,$data);//传入数据
    }
    curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
    $output = curl_exec($curl);
    curl_close($curl);

    return $output;
}