/*
 Navicat Premium Data Transfer

 Source Server         : 39.104.201.99
 Source Server Type    : MySQL
 Source Server Version : 50723
 Source Host           : localhost:33069
 Source Schema         : xg2019

 Target Server Type    : MySQL
 Target Server Version : 50723
 File Encoding         : 65001

 Date: 14/01/2019 11:35:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ft_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `ft_auth_group`;
CREATE TABLE `ft_auth_group`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自动编号',
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '角色名称',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态',
  `rules` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '权限编号组，逗号分割',
  `remark` varchar(600) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ft_auth_group
-- ----------------------------
INSERT INTO `ft_auth_group` VALUES (4, '运维管理员', 1, '71,72,73,65,66,67,74,75,58,68,78,79,69,70,63,64,53,62,55', '内管管理');
INSERT INTO `ft_auth_group` VALUES (9, '超级管理员', 1, '1,2,3,4,5,8,9,10,53,55,58,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89', '所有权限');
INSERT INTO `ft_auth_group` VALUES (10, '公司', 1, '', '测试');

-- ----------------------------
-- Table structure for ft_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `ft_auth_group_access`;
CREATE TABLE `ft_auth_group_access`  (
  `uid` mediumint(8) UNSIGNED NOT NULL COMMENT '用户编号',
  `group_id` mediumint(8) UNSIGNED NOT NULL COMMENT '角色编号',
  UNIQUE INDEX `uid_group_id`(`uid`, `group_id`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE,
  INDEX `group_id`(`group_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色规则关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ft_auth_group_access
-- ----------------------------
INSERT INTO `ft_auth_group_access` VALUES (1, 4);
INSERT INTO `ft_auth_group_access` VALUES (2, 4);
INSERT INTO `ft_auth_group_access` VALUES (3, 5);
INSERT INTO `ft_auth_group_access` VALUES (4, 6);
INSERT INTO `ft_auth_group_access` VALUES (5, 7);
INSERT INTO `ft_auth_group_access` VALUES (6, 8);
INSERT INTO `ft_auth_group_access` VALUES (7, 5);
INSERT INTO `ft_auth_group_access` VALUES (8, 6);
INSERT INTO `ft_auth_group_access` VALUES (9, 10);

-- ----------------------------
-- Table structure for ft_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `ft_auth_rule`;
CREATE TABLE `ft_auth_rule`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自动编号',
  `name` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'url表达式',
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '权限名称',
  `type` tinyint(4) UNSIGNED NOT NULL DEFAULT 1 COMMENT '类型',
  `status` tinyint(4) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  `condition` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '条件',
  `pid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上级编号',
  `sort` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序号',
  `isMenu` tinyint(4) UNSIGNED NOT NULL DEFAULT 2 COMMENT '是否菜单',
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'fa-th-large' COMMENT '菜单图标',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 93 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ft_auth_rule
-- ----------------------------
INSERT INTO `ft_auth_rule` VALUES (1, 'index/rule', '权限管理', 1, 1, '', 0, 99, 1, 'fa-shield');
INSERT INTO `ft_auth_rule` VALUES (2, 'index/rule/index', '权限列表', 1, 1, '', 1, 1, 1, 'fa-th-large');
INSERT INTO `ft_auth_rule` VALUES (3, 'index/role/index', '角色列表', 1, 1, '', 1, 2, 1, 'fa-th-large');
INSERT INTO `ft_auth_rule` VALUES (4, 'index/user/index', '用户管理', 1, 1, '', 1, 3, 1, 'fa-th-large');
INSERT INTO `ft_auth_rule` VALUES (5, 'index/dept/index', '部门管理', 1, 1, '', 1, 4, 1, 'fa-th-large');
INSERT INTO `ft_auth_rule` VALUES (8, 'index/rule/add', '新增', 1, 1, '', 2, 1, 2, 'fa-flash');
INSERT INTO `ft_auth_rule` VALUES (9, 'index/rule/del', '删除', 1, 1, '', 2, 2, 2, 'fa-flash');
INSERT INTO `ft_auth_rule` VALUES (10, 'index/rule/update', '修改', 1, 1, '', 2, 3, 2, 'fa-flash');
INSERT INTO `ft_auth_rule` VALUES (68, 'index/link/add', '新增', 1, 1, '', 62, 1, 2, '链接');
INSERT INTO `ft_auth_rule` VALUES (69, 'index/link/delete', '删除', 1, 1, '', 62, 2, 2, '');
INSERT INTO `ft_auth_rule` VALUES (70, 'index/link/update', '修改', 1, 1, '', 62, 3, 2, '');
INSERT INTO `ft_auth_rule` VALUES (78, 'index/link/add_data', '新增保存', 1, 1, '', 62, 1, 2, '');
INSERT INTO `ft_auth_rule` VALUES (79, 'index/link/update_data', '修改保存', 1, 1, '', 62, 1, 2, '');
INSERT INTO `ft_auth_rule` VALUES (80, 'index/role/add', '新增', 1, 1, '', 3, 1, 2, '');
INSERT INTO `ft_auth_rule` VALUES (81, 'index/role/update', '修改', 1, 1, '', 3, 1, 2, '');
INSERT INTO `ft_auth_rule` VALUES (82, 'index/role/del', '删除', 1, 1, '', 3, 1, 2, '');
INSERT INTO `ft_auth_rule` VALUES (83, 'index/role/get', '选取角色', 1, 1, '', 3, 1, 2, '');
INSERT INTO `ft_auth_rule` VALUES (84, 'index/user/add', '新增', 1, 1, '', 4, 1, 2, '');
INSERT INTO `ft_auth_rule` VALUES (85, 'index/user/update', '修改', 1, 1, '', 4, 1, 2, '');
INSERT INTO `ft_auth_rule` VALUES (86, 'index/user/get', '获取用户信息', 1, 1, '', 4, 1, 2, '');
INSERT INTO `ft_auth_rule` VALUES (87, 'index/cate/add', '新增', 1, 1, '', 5, 1, 2, '');
INSERT INTO `ft_auth_rule` VALUES (88, 'index/cate/update', '修改', 1, 1, '', 5, 1, 2, '');
INSERT INTO `ft_auth_rule` VALUES (89, 'index/cate/del', '删除', 1, 1, '', 5, 1, 2, '');
INSERT INTO `ft_auth_rule` VALUES (90, 'index/site/add', '新增', 1, 1, '', 58, 1, 2, '');
INSERT INTO `ft_auth_rule` VALUES (91, 'index/site/update', '更新', 1, 1, '', 58, 2, 2, '');
INSERT INTO `ft_auth_rule` VALUES (92, 'index/site/del', '删除', 1, 1, '', 58, 3, 2, '');

-- ----------------------------
-- Table structure for ft_dept
-- ----------------------------
DROP TABLE IF EXISTS `ft_dept`;
CREATE TABLE `ft_dept`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自动编号',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `cate` tinyint(4) UNSIGNED NOT NULL DEFAULT 1 COMMENT '类别',
  `remark` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ft_dept
-- ----------------------------
INSERT INTO `ft_dept` VALUES (1, '环境监测中心', 1, '环境监测中心', '2018-07-26 17:24:10', '2018-12-19 17:19:08');

-- ----------------------------
-- Table structure for ft_dict
-- ----------------------------
DROP TABLE IF EXISTS `ft_dict`;
CREATE TABLE `ft_dict`  (
  `cate` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典表类型',
  `id` int(11) NOT NULL COMMENT '字典编号',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典值',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注，扩展用',
  `status` tinyint(4) UNSIGNED NULL DEFAULT 1 COMMENT '状态 1 正常 2 停用',
  UNIQUE INDEX `uidx_dict`(`cate`, `id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ft_dict
-- ----------------------------
INSERT INTO `ft_dict` VALUES ('job_cate', 1, '日常维护', '工作类型', 1);
INSERT INTO `ft_dict` VALUES ('job_cate', 2, '应急检修', NULL, 1);
INSERT INTO `ft_dict` VALUES ('job_cate', 3, '电力故障维修', NULL, 1);
INSERT INTO `ft_dict` VALUES ('job_cate', 4, '附属设备故障维修', NULL, 1);
INSERT INTO `ft_dict` VALUES ('job_cate', 5, '人为干扰', NULL, 1);
INSERT INTO `ft_dict` VALUES ('job_cate', 6, '其它', NULL, 1);
INSERT INTO `ft_dict` VALUES ('link_cate', 1, '省属', '隶属', 1);
INSERT INTO `ft_dict` VALUES ('link_cate', 2, '县属', NULL, 1);

-- ----------------------------
-- Table structure for ft_notify
-- ----------------------------
DROP TABLE IF EXISTS `ft_notify`;
CREATE TABLE `ft_notify`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自动编号',
  `uid` int(11) NOT NULL COMMENT '用户id',
  `title` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '通知标题',
  `content` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '通知内容',
  `create_time` datetime(0) NOT NULL COMMENT '通知时间',
  `is_read` tinyint(3) UNSIGNED NOT NULL DEFAULT 2 COMMENT '是否已读 1 是 2 否',
  `read_time` datetime(0) NULL DEFAULT NULL COMMENT '已读时间',
  `url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '跳转url',
  `extra` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '扩展参数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 42 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '消息提醒' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ft_notify
-- ----------------------------
INSERT INTO `ft_notify` VALUES (38, 13, '购买通知', '石家庄市长安区化学品经营工厂购买氨100吨，出货日期2018-11-10，运输方式公路运输，由买方负责', '2018-11-06 09:28:47', 1, '2018-11-06 10:39:17', '/ent/transport/index.html', '');
INSERT INTO `ft_notify` VALUES (39, 13, '购买通知', '石家庄市长安区化学品经营工厂购买氨200吨，出货日期2018-11-10，运输方式公路运输，由买方负责', '2018-11-06 10:51:21', 1, '2018-11-06 11:37:08', '/ent/transport/index.html', '');
INSERT INTO `ft_notify` VALUES (40, 13, '购买通知', '石家庄市长安区化学品经营工厂购买氨50吨，出货日期2018-11-10，运输方式公路运输，由买方负责', '2018-11-06 11:37:35', 1, '2018-11-07 10:39:19', '/ent/transport/index.html', '');
INSERT INTO `ft_notify` VALUES (41, 3, '预警通知', '石家庄市长安区化学品经营工厂购买氨许可量超限75%', '2018-11-06 11:38:18', 2, NULL, '/index/warning/index.html', '');

-- ----------------------------
-- Table structure for ft_user
-- ----------------------------
DROP TABLE IF EXISTS `ft_user`;
CREATE TABLE `ft_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '编号，自动递增',
  `login_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录名',
  `passwd` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `remark` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `tel` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '手机号码',
  `create_time` datetime(0) NOT NULL COMMENT '自动时间戳-创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '自动时间戳-更新时间',
  `status` tinyint(4) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态标记 1 正常',
  `dept_id` int(10) UNSIGNED NOT NULL COMMENT '部门编号',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '姓名',
  `fail_cnt` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '失败次数',
  `pwd_date` datetime(0) NULL DEFAULT NULL COMMENT '最后一次修改密码时间',
  PRIMARY KEY (`id`, `login_name`, `tel`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '管理用户信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ft_user
-- ----------------------------
INSERT INTO `ft_user` VALUES (1, 'admin', 'f0d1b33c43d2a48751eab070649a8ada', '测试人员', '15131956793', '2018-07-26 17:25:02', '2019-01-14 08:27:39', 1, 1, '郭瑞涛', 0, '2018-12-14 09:33:42');
INSERT INTO `ft_user` VALUES (2, 'test', 'f0d1b33c43d2a48751eab070649a8ada', '测试人员', '13931171558', '2018-07-30 17:49:23', '2018-12-12 16:40:57', 1, 1, '测试', 0, '2018-12-13 13:55:10');
INSERT INTO `ft_user` VALUES (9, '托尔斯泰', '56f589f31538c5340e71d7076c480b44', '', '13933171556', '2018-11-14 10:39:11', '2018-12-21 14:05:36', 1, 8, 'test', 0, '2018-12-14 09:35:22');
INSERT INTO `ft_user` VALUES (11, 'test1214', 'f0d1b33c43d2a48751eab070649a8ada', '', '13933171554', '2018-12-14 09:54:49', '2018-12-14 09:54:49', 1, 1, 'test1214', 0, '2018-12-14 09:54:49');

SET FOREIGN_KEY_CHECKS = 1;
