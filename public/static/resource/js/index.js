let tel = /^1[3456789]\d{9}$/;
let tel2 = /^((0\d{2,3}-\d{7,8})|(1[3584]\d{9}))$/;

// 头部搜索
function howe () {
  $('.option-ul').toggle()
}
$('.option-ul li').click(function () {
  let oindex = $(this).index()
  let optionHtml = $('.option-ul li').eq(oindex).text()
  $('.option-sapn span').text(optionHtml)
  $('.option-ul').toggle()
});
$('.sousuo-text').click(function () {
  let ovalue = $('.sousuo-input').val();
  let odata = [$('.option-sapn span').text(), ovalue]
  console.log(odata)
  $.ajax({
    url: "http://39.106.186.232:8801/mgr/index/ceshi",
    type: "POST",
    data: {
      odata
    },
    dataType: "json",
    success: function (res) {
      console.log(res)
    },
    error: function () {

    }
  })
});
// 头部搜索 end


// 置顶
$(function () {
  $('.fiexd').hide();
  $(window).scroll(function () {
    if ($(this).scrollTop() > 300) {
      $('.fiexd').fadeIn();
    } else {
      $('.fiexd').fadeOut();
    }
  });
});
function goTop () {
  $('html ,body').animate({ scrollTop: 0 }, 200);
  return false;
}
// 置顶 end

function bannerBut () {
  let odata = $('#banner-form').serialize()
  let bannerName = $('#banenrName').val();
  let bannerTel = $('#banenrTel').val();
  let banenrName2 = $('#banenrName2').val();
  let banenrList = $('#banenrList').val();

  if (bannerName == "" || bannerTel == "" || banenrName2 == "") {
    alert('请输入完整信息！')
  } else if (!tel.test(bannerTel) && !tel2.test(bannerTel)) {
    alert('请输入正确手机号码！')
  } else if (banenrList == '装修类型') {
    alert('请选择装修类型！')
  } else {
    $.ajax({
      url: "http://39.106.186.232:8801/building/make",
      type: "POST",
      data: {
        odata
      },
      dataType: "json",
      success: function (res) {
        if (res.code == 0) {
          alert(res.msg);
        } else if (res.code == 1) {
          alert(res.msg);
        }
      },
      error: function () {

      }
    })
  }
}

// function baojia(){
// 	let odata = $('#baojia').serialize()
// 	let bjArea = $('#bjArea').val();
// 	let bjName = $('#bjName').val();
// 	let bjTel = $('#bjTel').val();
//
// 	if(bjArea == "" || bjName == "" || bjTel == ""){
// 		alert('请输入完整信息！')
// 	}else if(!tel.test(bjTel) && !tel2.test(bjTel)){
// 		alert('请输入正确手机号码！')
// 	}else{
// 		$.ajax({
// 			url: "http://39.106.186.232:8801/building/make",
// 			type: "POST",
// 			data: {
// 			   odata
// 			},
// 			dataType : "json",
// 			success: function (res) {
// 			   if (res.code == 0) {
// 			   	alert(res.msg);
// 			   }else if(res.code == 1){
// 			   	alert(res.msg);
// 			   }
// 			},
// 			error:function(){
//
// 			}
// 		})
// 	}
// }
function footerBut () {
  let odata = $('.footer-input').serialize()
  let footerName = $('#footerName').val();
  let footerTel = $('#footerTel').val();
  if (footerName == "" || footerTel == "") {
    alert('请输入完整信息！')
  } else if (!tel.test(footerTel) && !tel2.test(footerTel)) {
    alert('请输入正确手机号码！')
  } else {
    $.ajax({
      url: "http://39.106.186.232:8801/building/make",
      type: "POST",
      data: {
        odata
      },
      dataType: "json",
      success: function (res) {
        if (res.code == 0) {
          alert(res.msg);
        } else if (res.code == 1) {
          alert(res.msg);
        }
      },
      error: function () {

      }
    })
  }
}



// 报价页

$('#gongzhuang').hide()
$('.decorate_type').click(function () {
  let decorate_type = $(this).val()
  if (decorate_type == '家装') {
    $('#jiazhuang').show()
    $('#gongzhuang').hide()
  } else if (decorate_type == '工装') {
    $('#jiazhuang').hide()
    $('#gongzhuang').show()
  }
})

$('.alert-offer img').click(function () {
  $('.alert-offer').hide()
})
// 报价页 end

$('.list-none').hide()
let casesLi = $('.cases-ul li')
if (casesLi.length <= 0) {
  $('.paging-page').hide()
  $('.list-none').show()
}
$('.page-but').click(function () {
  let odata = $('#casesRight').serialize()
  let casesName = $('.casesName').val();
  let casesTel = $('.casesTel').val();
  let casesArea = $('.casesArea').val();
  if (casesName == "" || casesTel == "" || casesArea == "") {
    alert('请输入完整信息！')
  } else if (!tel.test(casesTel) && !tel2.test(casesTel)) {
    alert('请输入正确手机号码！')
  } else {
    $.ajax({
      url: "",
      type: "POST",
      data: {
        odata
      },
      dataType: "json",
      success: function (res) {
        if (res.code == 0) {
          alert(res.msg);
        } else if (res.code == 1) {
          alert(res.msg);
        }
      },
      error: function () {

      }
    })
  }
})
//导航出现
var width = $(window).width()

if (width <= 768) {  //  屏宽768触发
  $(document).click(function () {
    $('.mhm-header').find("nav>ul").hide();
  });                                  //  隐去一部分
} else {
  $(document).click(function () {
    $('.mhm-header').find("nav>ul").show();
    $('.mhm-header').find("nav>ul").css("display", "flex")
  });
  $('.mhm-header').find("nav>ul").show();
  $('.mhm-header').find("nav>ul").css("display", "flex")
}


$(".nav-head").on("click", "img", function (e) {
  e.stopPropagation();
  $('.mhm-header').find("nav>ul").show();
})
