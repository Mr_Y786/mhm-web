<?php /*a:1:{s:60:"/home/phpweb/zhanshi/application/index/view/login/index.html";i:1557132419;}*/ ?>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo htmlentities(app('config')->get('proj_name')); ?>-登录</title>

    <link href="/static/mgr/css/bootstrap.min.css" rel="stylesheet">
    <link href="/static/mgr/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/static/mgr/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="/static/mgr/css/animate.css" rel="stylesheet">
    <link href="/static/mgr/css/style.css" rel="stylesheet">

    <!-- Ladda style -->
    <link href="/static/mgr/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">




</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>

            <h1 class="logo-name">HB</h1>

        </div>
        <h2 style="font-weight: bold"><?php echo htmlentities(app('config')->get('proj_name')); ?></h2>
        <!--<p>Perfectly designed and precisely prepared admin theme with over 50 pages with extra new web app views.-->
            <!--Continually expanded and constantly improved Inspinia Admin Them (IN+)-->
        后台系统
        </p>
        <!--<p>Login in. To see it in action.</p>-->
        <form class="m-t" role="form" action="<?php echo url('index'); ?>" method="post">
            <div class="form-group">
                <input type="text" id="login_name" name="login_name" class="form-control" placeholder="用户名" required="">
            </div>
            <div class="form-group">
                <input type="password" id="passwd" name="passwd" class="form-control" placeholder="密码" required="">
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" id="captcha" name="captcha" class="form-control" placeholder="验证码" required="" onkeydown="submitForm()">
                    <span class="input-group-addon"><img id="verify_img" onclick="refreshVerify()" src="<?php echo captcha_src(); ?>" alt="captcha" style="height:34px;width: 100px;margin:-7px -12px;" /></span>
                </div>

            </div>
            <button type="button" id="btnSubmit" onclick="login()" class="btn btn-primary block full-width m-b">登&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;录</button>

            <!--<a href="#"><small>Forgot password?</small></a>-->
            <!--<p class="text-muted text-center"><small>Do not have an account?</small></p>-->
            <!--<a class="btn btn-sm btn-white btn-block" href="register.html">Create an account</a>-->
        </form>
        <p class="m-t"> <small></small> </p>
    </div>
</div>

<!-- Mainly scripts -->
<script src="/static/mgr/js/jquery-3.1.1.min.js"></script>
<script src="/static/mgr/js/bootstrap.min.js"></script>
<script src="/static/mgr/js/md5.js"></script>
<script src="/static/mgr/js/plugins/toastr/toastr.min.js"></script>
<!-- Ladda -->
<script src="/static/mgr/js/plugins/ladda/spin.min.js"></script>
<script src="/static/mgr/js/plugins/ladda/ladda.min.js"></script>

<script>
    var btnLadda;//ladda 保存按钮

    //toastr 初始化
    toastr.options = {
        closeButton: true,
        debug: false,
        progressBar: true,
        positionClass: "toast-top-center",
        onclick: null,
        showDuration: "300",
        hideDuration: "1000",
        timeOut: "2000",
        extendedTimeOut: "1000",
        showEasing: "swing",
        hideEasing: "linear",
        showMethod: "fadeIn",
        hideMethod: "fadeOut"
    };

    function refreshVerify() {
        var ts = Date.parse(new Date())/1000;
        $('#verify_img').attr("src", "/captcha?rand="+ts);
    }

    function login() {
        btnLadda.start();
        var passwd = $.trim($('#passwd').val());
        var captcha = $.trim($('#captcha').val());
        var loginName = $.trim($('#login_name').val());

        if(!loginName){
            toastr.warning('用户名称必填！');
            $('#login_name').focus();
            btnLadda.stop();
            return;
        }

        if(!passwd){
            toastr.warning('密码必填！');
            $('#passwd').focus();
            btnLadda.stop();
            return;
        }

        if(!captcha){
            toastr.warning('验证码必填！');
            $('#captcha').focus();
            btnLadda.stop();
            return;
        }

        var authKey = hex_md5(hex_md5(passwd)+captcha);

        $.post("<?php echo url('index'); ?>",
            {login_name:loginName,authKey:authKey,captcha:captcha},
            function(data){
            // console.log(data);
            // return ;
                if(data.code==0){
                    refreshVerify();
                    toastr.error(data.msg);
                    btnLadda.stop();
                }else{
                    window.location.href=data.url;
                }
            },
            "json"
        );
    }

    function submitForm(){
        if(event.keyCode ==13){
            login();
        }
    }

    $(document).ready(function() {
        btnLadda = Ladda.create(document.querySelector( '#btnSubmit' ) );
    });
</script>
</body>

</html>
