<?php /*a:1:{s:60:"/home/phpweb/zhanshi/application/index/view/tpl/success.html";i:1557111727;}*/ ?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo htmlentities(app('config')->get('proj_name')); ?>-操作成功</title>

    <link href="/static/mgr/css/bootstrap.min.css" rel="stylesheet">
    <link href="/static/mgr/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="/static/mgr/css/animate.css" rel="stylesheet">
    <link href="/static/mgr/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

<div class="loginColumns animated fadeInDown">
    <div class="row">
        <div class="panel panel-success">
            <div class="panel-heading">
                操作成功！
            </div>
            <div class="panel-body">
                <p><?php echo(strip_tags($msg));?></p>
            </div>
            <div class="panel-footer">
                页面自动 <a id="href" href="<?php echo($url);?>">跳转</a> 等待时间： <b id="wait"><?php echo($wait);?></b>
            </div>
        </div>

    </div>

</div>
<script type="text/javascript">
    (function(){
        var wait = document.getElementById('wait'),
            href = document.getElementById('href').href;
        var interval = setInterval(function(){
            var time = --wait.innerHTML;
            if(time <= 0) {
                location.href = href;
                clearInterval(interval);
            };
        }, 1000);
    })();
</script>
</body>

</html>
