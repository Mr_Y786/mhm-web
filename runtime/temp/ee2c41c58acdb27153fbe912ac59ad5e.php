<?php /*a:2:{s:59:"/home/phpweb/zhanshi/application/index/view/role/index.html";i:1557111727;s:55:"/home/phpweb/zhanshi/application/index/view/layout.html";i:1557136442;}*/ ?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo htmlentities(app('config')->get('proj_name')); ?>-管理</title>

    <link href="/static/mgr/css/bootstrap.min.css" rel="stylesheet">
    <link href="/static/mgr/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/static/mgr/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="/static/mgr/css/animate.css" rel="stylesheet">
    <link href="/static/mgr/css/style.css" rel="stylesheet">
    <script src="/static/mgr/js/jquery-3.1.1.min.js"></script>


</head>

<body>
<script>
    if(localStorage.getItem('navbarstatus')=='mini'){
        $('body').addClass('mini-navbar');
    }
</script>

<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear">
                                <span class="block m-t-xs">
                                    <strong class="font-bold"><?php echo htmlentities(app('session')->get('sess_user.sess_user_name')); ?></strong>
                                </span>
                                <span class="text-muted text-xs block">
                                    <?php echo htmlentities(app('session')->get('sess_user.sess_dept_name')); ?>
                                    <b class="caret"></b>
                                </span>
                            </span>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="<?php echo url('user/profile'); ?>">个人信息</a></li>
                            <li><a href="<?php echo url('user/updatepwd'); ?>">修改密码</a></li>
                            <!--<li><a href="mailbox.html">Mailbox</a></li>-->
                            <li class="divider"></li>
                            <li><a href="<?php echo url('index/login/logout'); ?>">退出</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        MHM
                    </div>
                </li>

                <?php if(is_array(app('session')->get('sess_user_menu')) || app('session')->get('sess_user_menu') instanceof \think\Collection || app('session')->get('sess_user_menu') instanceof \think\Paginator): $i = 0; $__LIST__ = app('session')->get('sess_user_menu');if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;if(!empty($vo['children'])): ?>
                <li <?php if(in_array(($vo['name']), is_array($pathInfoRange)?$pathInfoRange:explode(',',$pathInfoRange))): ?>class="active"<?php endif; ?>>
                <a href="#"><i class="fa <?php echo htmlentities($vo['icon']); ?>" title="<?php echo htmlentities($vo['title']); ?>"></i> <span class="nav-label"><?php echo htmlentities($vo['title']); ?></span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level" title="<?php echo htmlentities($vo['title']); ?>">
                    <?php if(is_array($vo['children']) || $vo['children'] instanceof \think\Collection || $vo['children'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vo['children'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$voc): $mod = ($i % 2 );++$i;?>
                    <li <?php if(in_array(($voc['name']), is_array($pathInfoRange)?$pathInfoRange:explode(',',$pathInfoRange))): ?>class="active"<?php endif; ?>>
                    <a href="<?php echo url($voc['name']); ?>"> <?php echo htmlentities($voc['title']); ?></a>
                    </li>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
                <!-- /.nav-second-level -->
                </li>

                <?php else: ?>
                <li <?php if(in_array(($vo['name']), is_array($pathInfoRange)?$pathInfoRange:explode(',',$pathInfoRange))): ?>class="active"<?php endif; ?>>
                <a href="<?php echo url($vo['name']); ?>"><i class="fa <?php echo htmlentities($vo['icon']); ?>"  title="<?php echo htmlentities($vo['title']); ?>"></i> <span class="nav-label"><?php echo htmlentities($vo['title']); ?></span></a>
                </li>
                <?php endif; ?>

                <?php endforeach; endif; else: echo "" ;endif; ?>

            </ul>

        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    <form role="search" class="navbar-form-custom" action="">
                        <div class="form-group">
                            <!--<input type="text" placeholder="" class="form-control" name="top-search" id="top-search">-->
                        </div>
                    </form>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li><a href="<?php echo url('index/index'); ?>">主页</a></li>
                    <li>
                        <!--<span class="m-r-sm text-muted welcome-message">暂无提醒</span>-->
                    </li>

                    <li class="dropdown">
                        <!--<a class="dropdown-toggle count-info" href="<?php echo url('index/notify/index'); ?>">-->
                        <a class="dropdown-toggle count-info" href="#">
                            <i class="fa fa-bell"></i>
                            <?php if($notifyCnt > '0'): ?>
                            <span class="label label-danger" id="num">
                                    <?php echo htmlentities($notifyCnt); ?>
                                </span>
                            <?php endif; ?>
                        </a>

                    </li>


                    <li>
                        <a href="<?php echo url('login/logout'); ?>">
                            <i class="fa fa-sign-out"></i> 退出
                        </a>
                    </li>

                </ul>

            </nav>
        </div>

        <div class="row">
            <div class="col-lg-12" id="divLayoutMainOp">
            
<!-- Ladda style -->
<link href="/static/mgr/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

<!-- Ladda -->
<script src="/static/mgr/js/plugins/ladda/spin.min.js"></script>
<script src="/static/mgr/js/plugins/ladda/ladda.min.js"></script>

<link href="/static/mgr/css/plugins/jsTree/style.min.css" rel="stylesheet">
<script src="/static/mgr/js/plugins/jsTree/jstree.min.js"></script>


<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-4 col-md-4 col-sm-4">
        <h3>角色列表</h3>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-8" style="text-align: right">
        <button type="button" class="btn btn-primary" onclick="addRole();">新增</button>&nbsp;&nbsp;
        <button type="button" class="btn btn-danger" onclick="delRole();">删除</button>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="ibox-content">

        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th width="50"><input type="checkbox" class="ftkj_chkbox" id="cbAll" onclick="checkAll()"></th>
                    <th width="100">名称</th>
                    <th width="300">描述</th>
                    <th width="80">操作</th>
                </tr>
                </thead>
                <tbody>

                <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                <tr>
                    <td><input type="checkbox" class="ftkj_chkbox" name="cbID" value="<?php echo htmlentities($vo['id']); ?>"></td>
                    <td><?php echo htmlentities($vo['title']); ?></td>
                    <td><?php echo htmlentities($vo['remark']); ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="editRole(<?php echo htmlentities($vo['id']); ?>)">编辑</a>
                    </td>
                </tr>
                <?php endforeach; endif; else: echo "" ;endif; ?>

                </tbody>
            </table>
        </div>

    </div>
</div>

<!--角色详情div-->
<div class="modal fade" id="opModal" tabindex="-1" role="dialog" aria-labelledby="opModalLabel" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="opModalLabel">角色信息</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal " role="form" id="opForm" action="" method="post">
                    <div class="form-group input-sm">
                        <label for="title" class="col-sm-3 control-label">角色名称：</label>
                        <div class="col-sm-9">
                            <input class="form-control" id="title" name="title" required="required">
                        </div>
                    </div>
                    <div class="form-group  input-sm">
                        <label for="remark" class="col-sm-3 control-label">角色备注：</label>
                        <div class="col-sm-9">
                            <input class="form-control" id="remark" name="remark" required="required">
                        </div>
                    </div>
                    <div class="form-group input-sm" style="height: 300px;">
                        <label class="col-sm-3 control-label">权限：</label>
                        <div class="col-sm-9">
                            <div id="divJsTree" class="form-control" style="height: 300px;overflow-y: scroll;"></div>
                        </div>
                    </div>
                    <input type="hidden" name="rules" id="rules">
                    <input type="hidden" name="id" id="id">
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="ladda-button btn btn-info" id="btnSave" onclick="save();" data-style="expand-right">保存</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>


<script>
    function checkAll() {
        $("tbody input[type='checkbox']").prop('checked',$('#cbAll').prop('checked'));
    }

    var addUrl="<?php echo url('role/add'); ?>";
    var updateUrl="<?php echo url('role/update'); ?>";

    var isAdd=true;
    var btnLadda;//ladda 保存按钮

    //增加
    function addRole(){
        //新增，清理选项内容
        $('#remark').val('');
        $('#title').val('');

        //获取默认节点
        $('#divJsTree').jstree().open_all();
        $('#divJsTree').jstree().uncheck_all();

        //按钮状态
        btnLadda.stop();

        $('#opModal').modal('show');
    }

    function editRole(roleid){
        //修改,设置选项
        $.getJSON("<?php echo url('role/get'); ?>?id="+roleid,function(res){
            $('#remark').val(res.remark);
            $('#title').val(res.title);

            //设置树形数据
            userTreeRuleJson = res.treeRule;

            $('#divJsTree').jstree(true).settings.core.data=userTreeRuleJson;
            $('#divJsTree').jstree(true).refresh(false,true);
            $('#divJsTree').jstree('open_all');

            $('#id').val(roleid);
            //按钮状态
            btnLadda.stop();
            isAdd=false;



            $('#opModal').modal('show');
        });

    }

    function delRole(ruleid){
        var cbIDs=[];
        $('input[name="cbID"]:checked').each(function(){
            cbIDs.push($(this).val());
        });

        if(cbIDs.length <=0){
            return ;
        }
        if (window.confirm('确认删除？')) {
            $.post("<?php echo url('role/del'); ?>",
                {ids:cbIDs.join(',')},
                function(data){
                    alert(data.msg);
                    if(data.code==0){
                        btnLadda.stop();
                    }else{
                        //刷新本页
                        window.location.reload();
                    }
                },
                "json"
            );
        }
    }

    //保存
    function save(){
        //tree test
        var allCheck = $('#divJsTree').jstree(true).get_checked(false); //获取所有选中的节点对象
        var allUndeterminedCheck = $('#divJsTree').jstree(true).get_undetermined(false); //获取所有半选中的节点对象
        var allNodes = allCheck.concat(allUndeterminedCheck);
        console.log(allNodes);
        //return;
        $('#rules').val(allNodes.join(','));

        btnLadda.start();

        var actionUrl = addUrl;
        if(!isAdd){
            actionUrl = updateUrl;
        }

        $.post(actionUrl,
            $('#opForm').serialize(),
            function(data){
                console.log(data);
                alert(data.msg);
                if(data.code==0){
                    btnLadda.stop();
                }else{
                    //刷新本页
                    window.location.reload();
                }
            },
            "json"
        );
    }


    var treeRuleJson=<?php echo $treeRuleJson; ?>;
    var userTreeRuleJson='';

    $(document).ready(function() {
        btnLadda = Ladda.create(document.querySelector( '#btnSave' ) );

        $('#divJsTree').jstree({
            "core":{"data":treeRuleJson},
            "plugins" : [ "checkbox" ],
            'state': {
                "opened":true,
            },
        });


    });
</script>
            </div>
        </div>

    </div>

</div>

<!-- Mainly scripts -->

<script src="/static/mgr/js/bootstrap.min.js"></script>
<script src="/static/mgr/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="/static/mgr/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="/static/mgr/js/plugins/toastr/toastr.min.js"></script>
<!-- Custom and plugin javascript -->
<script src="/static/mgr/js/inspinia.js"></script>

<script>

    $('.navbar-minimalize').on('click',function(){
        if($('body').hasClass('mini-navbar')){
            localStorage.setItem('navbarstatus', 'normal');
        }else{
            localStorage.setItem('navbarstatus', 'mini');
        }
    });

    //定时获取消息通知
    function getnums() {
        $.post("<?php echo url('mgr/index/notify'); ?>",function(data){
            if (data != 0){
                $('#num').text(data);
            }
        })
    }
    $(document).ready(function(){
        //setInterval(getnums, 30000);
    });
</script>


</body>
</html>
