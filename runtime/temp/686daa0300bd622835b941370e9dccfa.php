<?php /*a:2:{s:63:"/home/phpweb/zhanshi/application/index/view/user/updatepwd.html";i:1547193842;s:55:"/home/phpweb/zhanshi/application/index/view/layout.html";i:1547193842;}*/ ?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo htmlentities(app('config')->get('proj_name')); ?>-管理</title>

    <link href="/static/mgr/css/bootstrap.min.css" rel="stylesheet">
    <link href="/static/mgr/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/static/mgr/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="/static/mgr/css/animate.css" rel="stylesheet">
    <link href="/static/mgr/css/style.css" rel="stylesheet">
    <script src="/static/mgr/js/jquery-3.1.1.min.js"></script>


</head>

<body>
<script>
    if(localStorage.getItem('navbarstatus')=='mini'){
        $('body').addClass('mini-navbar');
    }
</script>

<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo htmlentities(app('session')->get('sess_user.sess_user_name')); ?></strong>
                             </span> <span class="text-muted text-xs block"><?php echo htmlentities(app('session')->get('sess_user.sess_dept_name')); ?> <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="<?php echo url('user/profile'); ?>">个人信息</a></li>
                            <li><a href="<?php echo url('user/updatepwd'); ?>">修改密码</a></li>
                            <!--<li><a href="mailbox.html">Mailbox</a></li>-->
                            <li class="divider"></li>
                            <li><a href="<?php echo url('index/login/logout'); ?>">退出</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        FT+
                    </div>
                </li>

                <?php if(is_array(app('session')->get('sess_user_menu')) || app('session')->get('sess_user_menu') instanceof \think\Collection || app('session')->get('sess_user_menu') instanceof \think\Paginator): $i = 0; $__LIST__ = app('session')->get('sess_user_menu');if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;if(!empty($vo['children'])): ?>
                <li <?php if(in_array(($vo['name']), is_array($pathInfoRange)?$pathInfoRange:explode(',',$pathInfoRange))): ?>class="active"<?php endif; ?>>
                <a href="#"><i class="fa <?php echo htmlentities($vo['icon']); ?>" title="<?php echo htmlentities($vo['title']); ?>"></i> <span class="nav-label"><?php echo htmlentities($vo['title']); ?></span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level" title="<?php echo htmlentities($vo['title']); ?>">
                    <?php if(is_array($vo['children']) || $vo['children'] instanceof \think\Collection || $vo['children'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vo['children'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$voc): $mod = ($i % 2 );++$i;?>
                    <li <?php if(in_array(($voc['name']), is_array($pathInfoRange)?$pathInfoRange:explode(',',$pathInfoRange))): ?>class="active"<?php endif; ?>>
                    <a href="<?php echo url($voc['name']); ?>"> <?php echo htmlentities($voc['title']); ?></a>
                    </li>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
                <!-- /.nav-second-level -->
                </li>

                <?php else: ?>
                <li <?php if(in_array(($vo['name']), is_array($pathInfoRange)?$pathInfoRange:explode(',',$pathInfoRange))): ?>class="active"<?php endif; ?>>
                <a href="<?php echo url($vo['name']); ?>"><i class="fa <?php echo htmlentities($vo['icon']); ?>"  title="<?php echo htmlentities($vo['title']); ?>"></i> <span class="nav-label"><?php echo htmlentities($vo['title']); ?></span></a>
                </li>
                <?php endif; ?>

                <?php endforeach; endif; else: echo "" ;endif; ?>

            </ul>

        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    <form role="search" class="navbar-form-custom" action="">
                        <div class="form-group">
                            <!--<input type="text" placeholder="" class="form-control" name="top-search" id="top-search">-->
                        </div>
                    </form>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li><a href="<?php echo url('index/index'); ?>">主页</a></li>
                    <li>
                        <!--<span class="m-r-sm text-muted welcome-message">暂无提醒</span>-->
                    </li>

                    <li class="dropdown">
                        <!--<a class="dropdown-toggle count-info" href="<?php echo url('index/notify/index'); ?>">-->
                        <a class="dropdown-toggle count-info" href="#">
                            <i class="fa fa-bell"></i>
                            <?php if($notifyCnt > '0'): ?>
                            <span class="label label-danger" id="num">
                                    <?php echo htmlentities($notifyCnt); ?>
                                </span>
                            <?php endif; ?>
                        </a>

                    </li>


                    <li>
                        <a href="<?php echo url('login/logout'); ?>">
                            <i class="fa fa-sign-out"></i> 退出
                        </a>
                    </li>

                </ul>

            </nav>
        </div>

        <div class="row">
            <div class="col-lg-12" id="divLayoutMainOp">
            
<!-- Ladda style -->
<link href="/static/mgr/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

<!-- Ladda -->
<script src="/static/mgr/js/plugins/ladda/spin.min.js"></script>
<script src="/static/mgr/js/plugins/ladda/ladda.min.js"></script>


<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-4 col-md-4 col-sm-4">
        <h3>修改密码</h3>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-8" style="text-align: right">

    </div>
</div>
<div class="wrapper wrapper-content">

    <div class="ibox-content">
        <form method="post" id="formOp" class="form-horizontal" action="<?php echo url('updatepwd'); ?>">
            <div class="form-group">
                <label class="col-sm-2 control-label">原密码</label>
                <div class="col-sm-4">
                    <input type="password" name="oriPwd" id="oriPwd" class="form-control">
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">新密码</label>
                <div class="col-sm-4"><input type="password" name="newPwd" id="newPwd" class="form-control" maxlength="50"></div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">新密码确认</label>
                <div class="col-sm-4"><input type="password" name="confirmPwd" id="confirmPwd" class="form-control" maxlength="50">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">验证码</label>
                <div class="col-sm-4">
                <div class="input-group">
                    <input type="text" id="captcha" name="captcha" class="form-control" placeholder="验证码" required="" onkeydown="submitForm()">
                    <span class="input-group-addon"><img id="verify_img" onclick="refreshVerify()" src="<?php echo captcha_src(); ?>" alt="captcha" style="height:34px;width: 100px;margin:-7px -12px;" /></span>
                </div>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-sm-4 col-sm-offset-2">
                    <button class="btn btn-primary" id="btnSave" type="button" onclick="save()">&nbsp;&nbsp;保&nbsp;&nbsp;&nbsp;&nbsp;存&nbsp;&nbsp;</button>
                </div>
            </div>
        </form>


    </div>
</div>


<script src="/static/mgr/js/md5.js"></script>

<script>
    var btnLadda;//ladda 保存按钮



    function refreshVerify() {
        var ts = Date.parse(new Date())/1000;
        $('#verify_img').attr("src", "/captcha?rand="+ts);
    }

    //保存
    function save(){

        btnLadda.start();

        var actionUrl = "<?php echo url('updatepwd'); ?>";

        var oriPwd = $('#oriPwd').val();
        var captcha = $('#captcha').val();

        oriPwd = hex_md5(hex_md5(oriPwd)+captcha);

        var res = /^(?=.*\d)(?=.*[a-zA-Z])(?=.*[~!@#$%^&*()_+`\-={}:";'<>?,.\/]).{8,}$/;
        if(res.test($('#newPwd').val()) && res.test($('#confirmPwd').val())){
            var newPwd = hex_md5($('#newPwd').val());
            var confirmPwd = hex_md5($('#confirmPwd').val());
        }else{
            alert('密码长度不少于八位，并且必须包含数字、字母和特殊字符！');
            btnLadda.stop();
            return false;
        }

        $.post(actionUrl,
            {oriPwd:oriPwd,captcha:captcha,newPwd:newPwd,confirmPwd:confirmPwd},
            function(data){
                //
                if(data.code==0){
                    refreshVerify();
                    toastr.error(data.msg);
                    btnLadda.stop();
                }else{
                    alert(data.msg);
                    //刷新本页
                    window.location.href=data.url;;
                }
            },
            "json"
        );
    }

    function submitForm(){
        if(event.keyCode ==13){
            save();
        }
    }

    $(document).ready(function() {
        //toastr 初始化
        toastr.options = {
            closeButton: true,
            debug: false,
            progressBar: true,
            positionClass: "toast-top-center",
            onclick: null,
            showDuration: "300",
            hideDuration: "1000",
            timeOut: "2000",
            extendedTimeOut: "1000",
            showEasing: "swing",
            hideEasing: "linear",
            showMethod: "fadeIn",
            hideMethod: "fadeOut"
        };

        btnLadda = Ladda.create(document.querySelector( '#btnSave' ) );
    });
</script>

            </div>
        </div>

    </div>

</div>

<!-- Mainly scripts -->

<script src="/static/mgr/js/bootstrap.min.js"></script>
<script src="/static/mgr/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="/static/mgr/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="/static/mgr/js/plugins/toastr/toastr.min.js"></script>
<!-- Custom and plugin javascript -->
<script src="/static/mgr/js/inspinia.js"></script>

<script>

    $('.navbar-minimalize').on('click',function(){
        if($('body').hasClass('mini-navbar')){
            localStorage.setItem('navbarstatus', 'normal');
        }else{
            localStorage.setItem('navbarstatus', 'mini');
        }
    });

    //定时获取消息通知
    function getnums() {
        $.post("<?php echo url('mgr/index/notify'); ?>",function(data){
            if (data != 0){
                $('#num').text(data);
            }
        })
    }
    $(document).ready(function(){
        //setInterval(getnums, 30000);
    });
</script>


</body>
</html>
