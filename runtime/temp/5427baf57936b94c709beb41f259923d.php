<?php /*a:2:{s:59:"/home/phpweb/zhanshi/application/index/view/rule/index.html";i:1557111727;s:55:"/home/phpweb/zhanshi/application/index/view/layout.html";i:1557136442;}*/ ?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo htmlentities(app('config')->get('proj_name')); ?>-管理</title>

    <link href="/static/mgr/css/bootstrap.min.css" rel="stylesheet">
    <link href="/static/mgr/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/static/mgr/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="/static/mgr/css/animate.css" rel="stylesheet">
    <link href="/static/mgr/css/style.css" rel="stylesheet">
    <script src="/static/mgr/js/jquery-3.1.1.min.js"></script>


</head>

<body>
<script>
    if(localStorage.getItem('navbarstatus')=='mini'){
        $('body').addClass('mini-navbar');
    }
</script>

<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear">
                                <span class="block m-t-xs">
                                    <strong class="font-bold"><?php echo htmlentities(app('session')->get('sess_user.sess_user_name')); ?></strong>
                                </span>
                                <span class="text-muted text-xs block">
                                    <?php echo htmlentities(app('session')->get('sess_user.sess_dept_name')); ?>
                                    <b class="caret"></b>
                                </span>
                            </span>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="<?php echo url('user/profile'); ?>">个人信息</a></li>
                            <li><a href="<?php echo url('user/updatepwd'); ?>">修改密码</a></li>
                            <!--<li><a href="mailbox.html">Mailbox</a></li>-->
                            <li class="divider"></li>
                            <li><a href="<?php echo url('index/login/logout'); ?>">退出</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        MHM
                    </div>
                </li>

                <?php if(is_array(app('session')->get('sess_user_menu')) || app('session')->get('sess_user_menu') instanceof \think\Collection || app('session')->get('sess_user_menu') instanceof \think\Paginator): $i = 0; $__LIST__ = app('session')->get('sess_user_menu');if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;if(!empty($vo['children'])): ?>
                <li <?php if(in_array(($vo['name']), is_array($pathInfoRange)?$pathInfoRange:explode(',',$pathInfoRange))): ?>class="active"<?php endif; ?>>
                <a href="#"><i class="fa <?php echo htmlentities($vo['icon']); ?>" title="<?php echo htmlentities($vo['title']); ?>"></i> <span class="nav-label"><?php echo htmlentities($vo['title']); ?></span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level" title="<?php echo htmlentities($vo['title']); ?>">
                    <?php if(is_array($vo['children']) || $vo['children'] instanceof \think\Collection || $vo['children'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vo['children'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$voc): $mod = ($i % 2 );++$i;?>
                    <li <?php if(in_array(($voc['name']), is_array($pathInfoRange)?$pathInfoRange:explode(',',$pathInfoRange))): ?>class="active"<?php endif; ?>>
                    <a href="<?php echo url($voc['name']); ?>"> <?php echo htmlentities($voc['title']); ?></a>
                    </li>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
                <!-- /.nav-second-level -->
                </li>

                <?php else: ?>
                <li <?php if(in_array(($vo['name']), is_array($pathInfoRange)?$pathInfoRange:explode(',',$pathInfoRange))): ?>class="active"<?php endif; ?>>
                <a href="<?php echo url($vo['name']); ?>"><i class="fa <?php echo htmlentities($vo['icon']); ?>"  title="<?php echo htmlentities($vo['title']); ?>"></i> <span class="nav-label"><?php echo htmlentities($vo['title']); ?></span></a>
                </li>
                <?php endif; ?>

                <?php endforeach; endif; else: echo "" ;endif; ?>

            </ul>

        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    <form role="search" class="navbar-form-custom" action="">
                        <div class="form-group">
                            <!--<input type="text" placeholder="" class="form-control" name="top-search" id="top-search">-->
                        </div>
                    </form>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li><a href="<?php echo url('index/index'); ?>">主页</a></li>
                    <li>
                        <!--<span class="m-r-sm text-muted welcome-message">暂无提醒</span>-->
                    </li>

                    <li class="dropdown">
                        <!--<a class="dropdown-toggle count-info" href="<?php echo url('index/notify/index'); ?>">-->
                        <a class="dropdown-toggle count-info" href="#">
                            <i class="fa fa-bell"></i>
                            <?php if($notifyCnt > '0'): ?>
                            <span class="label label-danger" id="num">
                                    <?php echo htmlentities($notifyCnt); ?>
                                </span>
                            <?php endif; ?>
                        </a>

                    </li>


                    <li>
                        <a href="<?php echo url('login/logout'); ?>">
                            <i class="fa fa-sign-out"></i> 退出
                        </a>
                    </li>

                </ul>

            </nav>
        </div>

        <div class="row">
            <div class="col-lg-12" id="divLayoutMainOp">
            
<!-- Ladda style -->
<link href="/static/mgr/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

<!-- Ladda -->
<script src="/static/mgr/js/plugins/ladda/spin.min.js"></script>
<script src="/static/mgr/js/plugins/ladda/ladda.min.js"></script>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h3>权限列表</h3>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">

                <div class="ibox-content">

                    <div class="dd" id="nestableAuth">
                        <ol class="dd-list">
                            <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo1): $mod = ($i % 2 );++$i;?>
                            <li class="dd-item">
                                <div class="dd-handle">
                                    <span class="pull-right"> <a href="javascript:void(0);" onclick="addRule(<?php echo htmlentities($vo1['id']); ?>,'<?php echo htmlentities($vo1['title']); ?>',0,true)">[增加]</a> <a href="javascript:void(0);" onclick="editRule(<?php echo htmlentities($vo1['id']); ?>,'<?php echo htmlentities($vo1['name']); ?>','<?php echo htmlentities($vo1['title']); ?>',<?php echo htmlentities($vo1['sort']); ?>,'<?php echo htmlentities($vo1['icon']); ?>',<?php echo htmlentities($vo1['isMenu']); ?>,<?php echo htmlentities($vo1['pid']); ?>,'根节点')">[修改]</a> <a href="javascript:void(0);" onclick="delRule(<?php echo htmlentities($vo1['id']); ?>)">[删除]</a> </span>
                                    <span class="label <?php if($vo1['isMenu'] == '1'): ?>label-info<?php endif; ?>"><i class="fa <?php if($vo1['isMenu'] == '1'): ?><?php echo htmlentities($vo1['icon']); else: ?>fa-flash<?php endif; ?>"></i></span> <?php echo htmlentities($vo1['sort']); ?>.<?php echo htmlentities($vo1['title']); ?> [<?php echo htmlentities($vo1['name']); ?>]
                                </div>
                                    <?php if(!empty($vo1['children'])): ?>
                                    <ol class="dd-list">
                                        <?php if(is_array($vo1['children']) || $vo1['children'] instanceof \think\Collection || $vo1['children'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vo1['children'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo2): $mod = ($i % 2 );++$i;?>
                                        <li class="dd-item">
                                            <div class="dd-handle">
                                                <span class="pull-right"> <a href="javascript:void(0);" onclick="addRule(<?php echo htmlentities($vo2['id']); ?>,'<?php echo htmlentities($vo2['title']); ?>',<?php echo htmlentities($vo1['id']); ?>,true)">[增加]</a> <a href="javascript:void(0);" onclick="editRule(<?php echo htmlentities($vo2['id']); ?>,'<?php echo htmlentities($vo2['name']); ?>','<?php echo htmlentities($vo2['title']); ?>',<?php echo htmlentities($vo2['sort']); ?>,'<?php echo htmlentities($vo2['icon']); ?>',<?php echo htmlentities($vo2['isMenu']); ?>,<?php echo htmlentities($vo2['pid']); ?>,'<?php echo htmlentities($vo1['title']); ?>')">[修改]</a> <a href="javascript:void(0);" onclick="delRule(<?php echo htmlentities($vo2['id']); ?>)">[删除]</a> </span>
                                                <span class="label <?php if($vo2['isMenu'] == '1'): ?>label-info<?php endif; ?>"><i class="fa <?php if($vo2['isMenu'] == '1'): ?><?php echo htmlentities($vo2['icon']); else: ?>fa-flash<?php endif; ?>"></i></span> <?php echo htmlentities($vo2['sort']); ?>.<?php echo htmlentities($vo2['title']); ?> [<?php echo htmlentities($vo2['name']); ?>]
                                            </div>
                                            <?php if(!empty($vo2['children'])): ?>
                                            <ol class="dd-list">
                                                <?php if(is_array($vo2['children']) || $vo2['children'] instanceof \think\Collection || $vo2['children'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vo2['children'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo3): $mod = ($i % 2 );++$i;?>
                                                <li class="dd-item">
                                                    <div class="dd-handle">
                                                        <span class="pull-right"> <a href="javascript:void(0);" onclick="addRule(<?php echo htmlentities($vo3['id']); ?>,'<?php echo htmlentities($vo3['title']); ?>',<?php echo htmlentities($vo2['id']); ?>,false)">[增加]</a> <a href="javascript:void(0);" onclick="editRule(<?php echo htmlentities($vo3['id']); ?>,'<?php echo htmlentities($vo3['name']); ?>','<?php echo htmlentities($vo3['title']); ?>',<?php echo htmlentities($vo3['sort']); ?>,'<?php echo htmlentities($vo3['icon']); ?>',<?php echo htmlentities($vo3['isMenu']); ?>,<?php echo htmlentities($vo3['pid']); ?>,'<?php echo htmlentities($vo2['title']); ?>')">[修改]</a> <a href="javascript:void(0);" onclick="delRule(<?php echo htmlentities($vo3['id']); ?>)">[删除]</a> </span>
                                                        <span class="label <?php if($vo3['isMenu'] == '1'): ?>label-info<?php endif; ?>"><i class="fa <?php if($vo3['isMenu'] == '1'): ?><?php echo htmlentities($vo3['icon']); else: ?>fa-flash<?php endif; ?>"></i></span> <?php echo htmlentities($vo3['sort']); ?>.<?php echo htmlentities($vo3['title']); ?> [<?php echo htmlentities($vo3['name']); ?>]
                                                    </div>
                                                    <!--第四层-->
                                                    <?php if(!empty($vo3['children'])): ?>
                                                    <ol class="dd-list">
                                                        <?php if(is_array($vo3['children']) || $vo3['children'] instanceof \think\Collection || $vo3['children'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vo3['children'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo4): $mod = ($i % 2 );++$i;?>
                                                        <li class="dd-item">
                                                            <div class="dd-handle">
                                                                <span class="pull-right"><a href="javascript:void(0)" onclick="editRule(<?php echo htmlentities($vo4['id']); ?>,'<?php echo htmlentities($vo4['name']); ?>','<?php echo htmlentities($vo4['title']); ?>',<?php echo htmlentities($vo4['sort']); ?>,'<?php echo htmlentities($vo4['icon']); ?>',2,<?php echo htmlentities($vo4['pid']); ?>,'<?php echo htmlentities($vo3['title']); ?>')">修改</a> <a href="javascript:void(0);" onclick="delRule(<?php echo htmlentities($vo4['id']); ?>)">[删除]</a> </span>
                                                                <span class="label"><i class="fa fa-flash"></i></span> <?php echo htmlentities($vo4['sort']); ?>.<?php echo htmlentities($vo4['title']); ?> [<?php echo htmlentities($vo4['name']); ?>]
                                                            </div>
                                                        </li>
                                                        <?php endforeach; endif; else: echo "" ;endif; ?>
                                                    </ol>
                                                    <?php endif; ?>
                                                </li>
                                                <?php endforeach; endif; else: echo "" ;endif; ?>
                                            </ol>
                                            <?php endif; ?>
                                        </li>
                                        <?php endforeach; endif; else: echo "" ;endif; ?>
                                    </ol>
                                    <?php endif; ?>
                            </li>
                            <?php endforeach; endif; else: echo "" ;endif; ?>
                        </ol>
                    </div>



                </div>

            </div>
        </div>
    </div>
</div>

<!--权限管理div-->
<div class="modal fade" id="opModal" tabindex="-1" role="dialog" aria-labelledby="opModalLabel" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="opModalLabel">权限信息</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal " role="form" id="opForm" action="" method="post">
                    <div class="form-group input-sm">
                        <label for="name" class="col-sm-3 control-label">节点标识：</label>
                        <div class="col-sm-9">
                            <input class="form-control" id="name" name="name" required="required">
                        </div>
                    </div>
                    <div class="form-group  input-sm">
                        <label for="name" class="col-sm-3 control-label">节点名称：</label>
                        <div class="col-sm-9">
                            <input class="form-control" id="title" name="title" required="required">
                        </div>
                    </div>
                    <div class="form-group input-sm">
                        <label for="name" class="col-sm-3 control-label">排序号：</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="number" id="sort" name="sort" required="required">
                        </div>
                    </div>
                    <div class="form-group input-sm">
                        <label for="name" class="col-sm-3 control-label">图标名称：</label>
                        <div class="col-sm-9">
                            <input class="form-control" id="icon" name="icon" placeholder="非菜单可为空">
                        </div>
                    </div>
                    <div class="form-group input-sm">
                        <label class="col-sm-3 control-label">是否菜单：</label>
                        <div class="col-sm-9 form-inline" id="divIsMenu">
                            <label><input type="radio" class="form-control" name="isMenu" value="1">&nbsp;&nbsp;是</label>&nbsp;&nbsp;&nbsp;&nbsp;
                            <label><input type="radio" class="form-control" name="isMenu" value="2" checked="checked">&nbsp;&nbsp;否</label>
                        </div>
                    </div>

                    <div class="form-group input-sm">
                        <label for="name" class="col-sm-3 control-label">父节点：</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="pid" name="pid">
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="id" id="id">
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="ladda-button btn btn-info" id="btnSave" onclick="save();" data-style="expand-right">保存</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>

<script>
    var addUrl="<?php echo url('rule/add'); ?>";
    var updateUrl="<?php echo url('rule/update'); ?>";

    var isAdd=true;
    var btnLadda;//ladda 保存按钮
    //增加节点
    function addRule(pid,pname,ppid,isAllowMenu){
        //新增，清理选项内容
        $('#name').val('');
        $('#title').val('');
        $('#sort').val(1);
        $('#icon').val('');
        $('#id').val('');
        $('#pid').empty();
        $('#pid').append("<option value='"+ppid+"'>同级别</option>");
        $('#pid').append("<option value='"+pid+"' selected='selected'>"+pname+"</option>");
        //若是第4级别，不可增加菜单选项
        if(isAllowMenu){
            //是否菜单设置为是
            $('#divIsMenu').find("input[type='radio']:first").prop('checked','checked');
        }else{
            //是否菜单设置为否，并且不可更改
            $('#divIsMenu').find("input[type='radio']:last").prop('checked','checked');
        }
        //按钮状态
        btnLadda.stop();
        isAdd=true;

        $('#opModal').modal('show');
    }

    function editRule(ruleid,rulename,ruletitle,rulesort,ruleicon,ruleisMenu,rulepid,rulepname){
        //修改，设置选项内容
        $('#name').val(rulename);
        $('#title').val(ruletitle);
        $('#sort').val(rulesort);
        $('#icon').val(ruleicon);
        $('#id').val(ruleid);
        $('#pid').empty();
        $('#pid').append("<option value='"+rulepid+"' selected='selected'>"+rulepname+"</option>");
        if(ruleisMenu==1){
            $('#divIsMenu').find("input[type='radio']:first").attr('checked','checked');
            $('#divIsMenu').find("input[type='radio']:last").removeAttr('checked');
        }else{
            $('#divIsMenu').find("input[type='radio']:last").attr('checked','checked');
            $('#divIsMenu').find("input[type='radio']:first").removeAttr('checked');
        }
        //按钮状态
        btnLadda.stop();
        isAdd=false;

        $('#opModal').modal('show');
    }

    function delRule(ruleid){
        if (window.confirm('确认删除？')) {
            window.location.href = "<?php echo url('rule/del'); ?>?ruleid=" + ruleid
        }
    }

    //保存
    function save(){
        btnLadda.start();

        var actionUrl = addUrl;
        if(!isAdd){
            actionUrl = updateUrl;
        }

        $.post(actionUrl,
            $('#opForm').serialize(),
            function(data){

                if(data.code==0){
                    alert(data.msg);
                    btnLadda.stop();
                }else{
                    //刷新本页
                    window.location.reload();
                }
            },
            "json"
        );
    }


    $(document).ready(function() {
        btnLadda = Ladda.create(document.querySelector( '#btnSave' ) );

    });
</script>
            </div>
        </div>

    </div>

</div>

<!-- Mainly scripts -->

<script src="/static/mgr/js/bootstrap.min.js"></script>
<script src="/static/mgr/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="/static/mgr/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="/static/mgr/js/plugins/toastr/toastr.min.js"></script>
<!-- Custom and plugin javascript -->
<script src="/static/mgr/js/inspinia.js"></script>

<script>

    $('.navbar-minimalize').on('click',function(){
        if($('body').hasClass('mini-navbar')){
            localStorage.setItem('navbarstatus', 'normal');
        }else{
            localStorage.setItem('navbarstatus', 'mini');
        }
    });

    //定时获取消息通知
    function getnums() {
        $.post("<?php echo url('mgr/index/notify'); ?>",function(data){
            if (data != 0){
                $('#num').text(data);
            }
        })
    }
    $(document).ready(function(){
        //setInterval(getnums, 30000);
    });
</script>


</body>
</html>
