<?php /*a:2:{s:59:"/home/phpweb/zhanshi/application/index/view/info/index.html";i:1557189301;s:55:"/home/phpweb/zhanshi/application/index/view/layout.html";i:1557136442;}*/ ?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo htmlentities(app('config')->get('proj_name')); ?>-管理</title>

    <link href="/static/mgr/css/bootstrap.min.css" rel="stylesheet">
    <link href="/static/mgr/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/static/mgr/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="/static/mgr/css/animate.css" rel="stylesheet">
    <link href="/static/mgr/css/style.css" rel="stylesheet">
    <script src="/static/mgr/js/jquery-3.1.1.min.js"></script>


</head>

<body>
<script>
    if(localStorage.getItem('navbarstatus')=='mini'){
        $('body').addClass('mini-navbar');
    }
</script>

<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear">
                                <span class="block m-t-xs">
                                    <strong class="font-bold"><?php echo htmlentities(app('session')->get('sess_user.sess_user_name')); ?></strong>
                                </span>
                                <span class="text-muted text-xs block">
                                    <?php echo htmlentities(app('session')->get('sess_user.sess_dept_name')); ?>
                                    <b class="caret"></b>
                                </span>
                            </span>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="<?php echo url('user/profile'); ?>">个人信息</a></li>
                            <li><a href="<?php echo url('user/updatepwd'); ?>">修改密码</a></li>
                            <!--<li><a href="mailbox.html">Mailbox</a></li>-->
                            <li class="divider"></li>
                            <li><a href="<?php echo url('index/login/logout'); ?>">退出</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        MHM
                    </div>
                </li>

                <?php if(is_array(app('session')->get('sess_user_menu')) || app('session')->get('sess_user_menu') instanceof \think\Collection || app('session')->get('sess_user_menu') instanceof \think\Paginator): $i = 0; $__LIST__ = app('session')->get('sess_user_menu');if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;if(!empty($vo['children'])): ?>
                <li <?php if(in_array(($vo['name']), is_array($pathInfoRange)?$pathInfoRange:explode(',',$pathInfoRange))): ?>class="active"<?php endif; ?>>
                <a href="#"><i class="fa <?php echo htmlentities($vo['icon']); ?>" title="<?php echo htmlentities($vo['title']); ?>"></i> <span class="nav-label"><?php echo htmlentities($vo['title']); ?></span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level" title="<?php echo htmlentities($vo['title']); ?>">
                    <?php if(is_array($vo['children']) || $vo['children'] instanceof \think\Collection || $vo['children'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vo['children'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$voc): $mod = ($i % 2 );++$i;?>
                    <li <?php if(in_array(($voc['name']), is_array($pathInfoRange)?$pathInfoRange:explode(',',$pathInfoRange))): ?>class="active"<?php endif; ?>>
                    <a href="<?php echo url($voc['name']); ?>"> <?php echo htmlentities($voc['title']); ?></a>
                    </li>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
                <!-- /.nav-second-level -->
                </li>

                <?php else: ?>
                <li <?php if(in_array(($vo['name']), is_array($pathInfoRange)?$pathInfoRange:explode(',',$pathInfoRange))): ?>class="active"<?php endif; ?>>
                <a href="<?php echo url($vo['name']); ?>"><i class="fa <?php echo htmlentities($vo['icon']); ?>"  title="<?php echo htmlentities($vo['title']); ?>"></i> <span class="nav-label"><?php echo htmlentities($vo['title']); ?></span></a>
                </li>
                <?php endif; ?>

                <?php endforeach; endif; else: echo "" ;endif; ?>

            </ul>

        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    <form role="search" class="navbar-form-custom" action="">
                        <div class="form-group">
                            <!--<input type="text" placeholder="" class="form-control" name="top-search" id="top-search">-->
                        </div>
                    </form>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li><a href="<?php echo url('index/index'); ?>">主页</a></li>
                    <li>
                        <!--<span class="m-r-sm text-muted welcome-message">暂无提醒</span>-->
                    </li>

                    <li class="dropdown">
                        <!--<a class="dropdown-toggle count-info" href="<?php echo url('index/notify/index'); ?>">-->
                        <a class="dropdown-toggle count-info" href="#">
                            <i class="fa fa-bell"></i>
                            <?php if($notifyCnt > '0'): ?>
                            <span class="label label-danger" id="num">
                                    <?php echo htmlentities($notifyCnt); ?>
                                </span>
                            <?php endif; ?>
                        </a>

                    </li>


                    <li>
                        <a href="<?php echo url('login/logout'); ?>">
                            <i class="fa fa-sign-out"></i> 退出
                        </a>
                    </li>

                </ul>

            </nav>
        </div>

        <div class="row">
            <div class="col-lg-12" id="divLayoutMainOp">
            

            </div>
        </div>

    </div>

</div>

<!-- Mainly scripts -->

<script src="/static/mgr/js/bootstrap.min.js"></script>
<script src="/static/mgr/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="/static/mgr/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="/static/mgr/js/plugins/toastr/toastr.min.js"></script>
<!-- Custom and plugin javascript -->
<script src="/static/mgr/js/inspinia.js"></script>

<script>

    $('.navbar-minimalize').on('click',function(){
        if($('body').hasClass('mini-navbar')){
            localStorage.setItem('navbarstatus', 'normal');
        }else{
            localStorage.setItem('navbarstatus', 'mini');
        }
    });

    //定时获取消息通知
    function getnums() {
        $.post("<?php echo url('mgr/index/notify'); ?>",function(data){
            if (data != 0){
                $('#num').text(data);
            }
        })
    }
    $(document).ready(function(){
        //setInterval(getnums, 30000);
    });
</script>


</body>
</html>
