<?php
/**
 * Created by PhpStorm.
 * User: ftkj
 * Date: 2018/11/9
 * Time: 16:01
 */

return [
    'length'=>4,
    'codeSet'=>'23456789abcdefhjkmnprstuvwxyz',
    'fontSize'=>48,
    'useCurve'=>false,
    'reset'=>true,

];
